import React, { useEffect, useState } from 'react'

function DurationTimer({ duration }) {
    const [timer, setTimer] = useState(0);

    useEffect(() => {
        setTimer(duration);
        const interval = setInterval(() => {
            setTimer((prevTimer) => prevTimer + 1);
        }, 1000);

        return () => clearInterval(interval);
    }, [duration]);

    // lebih dari 60 detik
    return <span className={`font-size-xs mt-2 ${timer > 60 ? 'text-danger' : 'text-muted'}`}>{FormatTimer(timer)}</span>
    // style={{ animation: 'animation-offcanvas-fade-in 1s linear infinite' }}>
}
// DurationTimer(3600);

function FormatTimer(duration) {
    let timer = duration;
    let hours, minutes, seconds;

    hours = parseInt(timer / 3600, 10);
    minutes = parseInt((timer % 3600) / 60, 10);
    seconds = parseInt((timer % 3600) % 60, 10);

    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    return `${hours} : ${minutes} : ${seconds}`;
}

export default DurationTimer