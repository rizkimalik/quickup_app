import React from 'react'

const FormGroup = ({ children, label, formtext }) => {
    return (
        <div className="form-group p-0 mb-4">
            <label>{label} :</label>
            {children}
            {formtext && <span className="form-text text-muted">{formtext}</span>}
        </div>
    )
}

export default FormGroup
