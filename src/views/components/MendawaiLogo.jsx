import React from 'react'
import logo_hitam from 'assets/image/logo-hitam.png'
import logo_putih from 'assets/image/logo-putih.png'

function MendawaiLogo({ className, colorLogo }) {
    if (colorLogo === 'white') {
        return <><img src={logo_putih} alt="Logo-Icon" className={className} /></>
    }
    else if(colorLogo === 'black') {
        return <><img src={logo_hitam} alt="Logo-Icon" className={className} /></>
    }
    else{
        return <><img src={logo_putih} alt="Logo-Icon" className={className} /></>
    }
}

export default MendawaiLogo
