import React, { useEffect } from 'react';
import { socket } from 'app/config';
import { useDispatch, useSelector } from 'react-redux';
import { ShowNotification } from 'views/components/Notification';
import { setSocketStatus } from 'app/slice/sliceSosmed';
import { authUser } from 'app/slice/sliceAuth';

const SocketIO = () => {
    const dispatch = useDispatch();
    const { username, user_level } = useSelector(authUser)
    console.log(user_level);
    useEffect(() => {
        if (user_level === 'L1' || user_level === 'SPV') { //? socket connect user L1 & SPV
            socket.auth = {
                flag_to: 'agent',
                username: username,
                email: ''
            }
            socket.connect();
            socket.on('connect', async function () {
                console.log(`connected: ${socket.connected}, id: ${socket.id}`);
                dispatch(setSocketStatus({
                    uuid: socket.id,
                    connected: socket.connected
                }))
            });
        }
    }, [dispatch, username, user_level]);

    useEffect(() => {
        socket.on('return-email-in', (data) => {
            ShowNotification(data);
        });
        socket.on('send-message-customer', (data) => { //push from blending
            ShowNotification(data);
        });
        socket.on('return-message-customer', (data) => {
            ShowNotification(data);
        });
        socket.on('return-message-whatsapp', (data) => {
            ShowNotification(data);
        });
        socket.on('return-directmessage-twitter', (data) => {
            ShowNotification(data);
        });
        socket.on('return-facebook-messenger', (data) => {
            ShowNotification(data);
        });
        socket.on('return-facebook-feed', (data) => {
            ShowNotification(data);
        });
        socket.on('return-instagram-messenger', (data) => {
            ShowNotification(data);
        });
        socket.on('return-instagram-feed', (data) => {
            ShowNotification(data);
        });
        socket.on('return-message-telegram', (data) => {
            ShowNotification(data);
        });
        socket.on('return-transfer-assign-agent', (data) => {
            ShowNotification(data);
        });

    }, [])

    return <></>;
}

export default SocketIO
