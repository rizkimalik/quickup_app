import Swal from "sweetalert2";

function AskPermission() {
    try {
        Notification.requestPermission().then((permission) => {
            console.log(`Notification permission : ${permission}`);
        });
    }
    catch (e) {
        return false;
    }
    return true;
}

function ShowNotification(data) {
    WebNotification(data);
    function PushNotification() {
        let options = {
            body: data.message,
            icon: '/assets/media/icons/notification.png',
            tag: data.message
        }
        const notif = new Notification(`${data.name} - ${data.channel}`, options);
        notif.onclick = function (event) {
            event.preventDefault();
            // location.href = `${DomainUrl}/inhealth/HTML/TrxShowTicket.aspx?ticketid=${Title}`;
        }
    }


    if (Notification.permission === 'granted') {
        PushNotification();
    } else if (Notification.permission !== 'denied') {
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
                PushNotification();
            }
        });
    }
}

function WebNotification(data) {
    Swal.fire({
        title: `${data.name} - ${data.channel}`,
        text: data.message,
        icon: 'info',
        position: 'top-start',
        showConfirmButton: false,
        timer: 5000,
        // backdrop: false,
        toast: true,
        timerProgressBar: true
    });
}

export { AskPermission, ShowNotification }