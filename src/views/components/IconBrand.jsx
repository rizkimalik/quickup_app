import React from 'react'
import chat from 'assets/icons/chat.png'
import email from 'assets/icons/email.png'
import messenger from 'assets/icons/messenger.png'
import twitter from 'assets/icons/twitter.png'
import facebook from 'assets/icons/facebook.png'
import whatsapp from 'assets/icons/whatsapp.png'
import instagram from 'assets/icons/instagram.png'
import call from 'assets/icons/call.png'
import telegram from 'assets/icons/telegram.png'
import omnichannel from 'assets/icons/omnichannel.png'

function IconBrand({ name, height, width }) {
    if (name === 'Chat') {
        return <div><img src={chat} height={height} width={width} alt="chat" /></div>;
    } else if (name === 'Email') {
        return <div><img src={email} height={height} width={width} alt="email" /></div>;
    } else if (name === 'Messenger') {
        return <div><img src={messenger} height={height} width={width} alt="messenger" /></div>;
    } else if (name === 'Twitter') {
        return <div><img src={twitter} height={height} width={width} alt="twitter" /></div>;
    } else if (name === 'Facebook') {
        return <div><img src={facebook} height={height} width={width} alt="facebook" /></div>;
    } else if (name === 'Whatsapp') {
        return <div><img src={whatsapp} height={height} width={width} alt="whatsapp" /></div>;
    } else if (name === 'Instagram') {
        return <div><img src={instagram} height={height} width={width} alt="instagram" /></div>;
    } else if (name === 'Call') {
        return <div><img src={call} height={height} width={width} alt="call" /></div>;
    } else if (name === 'Telegram') {
        return <div><img src={telegram} height={height} width={width} alt="telegram" /></div>;
    } else if (name === 'All') {
        return <div><img src={omnichannel} height={height} width={width} alt="omnichannel" /></div>;
    }
    else {
        return '';
    }
}

export default IconBrand