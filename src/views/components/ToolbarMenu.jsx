import React from 'react'
import { NavLink } from 'react-router-dom'

function ToolbarMenu() {
    return (
        <div className="dropdown dropdown-inline">
            <button className="btn btn-sm btn-light-primary btn-icon" data-toggle="dropdown" aria-expanded="false">
                {/* <IconSetting className="svg-icon svg-icon-md" /> */}
                icon
            </button>
            <div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <ul className="navi flex-column navi-hover py-2">
                    {/* <li className="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Choose an action: </li> */}
                    <li className="navi-item">
                        <div data-toggle="modal" data-target="#modalJourneyCustomer" className="navi-link" style={{ cursor: 'pointer' }}>
                            <i className="fas fa-route navi-icon"></i> Customer Journey
                        </div>
                    </li>
                    <li className="navi-item">
                        <NavLink to="/" className="navi-link">
                            Customer Detail
                        </NavLink>
                    </li>
                    <li className="navi-item">
                        <div className="dropdown-divider"></div>
                    </li>
                    <li className="navi-item">
                        <NavLink to="/" className="navi-link" data-toggle="modal" data-target="#modalCustomerSyncronize">
                            Syncronize To
                        </NavLink>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default ToolbarMenu