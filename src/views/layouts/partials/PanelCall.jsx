import React from 'react'

function PanelCall() {
    return (
        <div className="modal modal-sticky modal-sticky-bottom-right show" id="kt_chat_modal" data-backdrop="false" style={{ display: 'block', paddingRight: 17 }} aria-modal="true" role="dialog">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="card card-custom">
                        <div className="card-header align-items-center px-4 py-3">
                            <div className="text-left flex-grow-1"></div>
                            <div className="text-center flex-grow-1">
                                <div className="text-dark-75 font-weight-bold font-size-h5">Matt Pears</div>
                                <div>
                                    <span className="label label-dot label-success" />
                                    <span className="font-weight-bold text-muted font-size-sm">Active</span>
                                </div>
                            </div>
                            <div className="text-right flex-grow-1">
                                <button type="button" className="btn btn-clean btn-sm btn-icon btn-icon-md" data-dismiss="modal">
                                    <i className="ki ki-close icon-1x" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="scroll scroll-pull ps ps--active-y" data-height={375} data-mobile-height={300} style={{ height: 375, overflow: 'hidden' }}>
                                <p>content</p>
                            </div>
                        </div>
                        <div className="card-footer align-items-center">
                            <div className="d-flex align-items-center justify-content-between">
                                <button type="button" className="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default PanelCall