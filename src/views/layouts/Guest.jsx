import React from 'react';
import { Link } from 'react-router-dom';
import MendawaiLogo from 'views/components/MendawaiLogo';

function Guest({ children }) {
    return (
        <div className="flex-row align-items-center vh-100 bg-guest">
            <div className="container">
                <div className="justify-content-center row">
                    <div className="col-md-4">
                        <div className="d-flex flex-center mb-15 mt-20">
                            <Link to="/">
                                <MendawaiLogo colorLogo="black" className="max-h-75px" />
                            </Link>
                        </div>
                        {children}
                    </div>
                </div>
                <div className="d-flex justify-content-center align-items-center py-7 py-lg-0">
                    <a href="https://quickup.id/privacy-policy" target="_blank" className="text-primary font-weight-bolder font-size-lg">Privacy Policy</a>
                    <a href="https://quickup.id" target="_blank" className="text-primary ml-5 font-weight-bolder font-size-lg">About Us</a>
                </div>
            </div>
        </div>
    )
}

export default Guest
