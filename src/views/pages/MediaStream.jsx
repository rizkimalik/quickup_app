import React, { useEffect, useRef } from 'react'
// import mp4 from 'assets/100100.mp4';
// import wav from 'assets/1000.wav';

function MediaStream() {
    const refMyVideo = useRef();

    useEffect(() => {
        function getMedia() {
            navigator.mediaDevices.getUserMedia({ video: false, audio: true })
                .then((stream) => {
                    // setStream(stream)
                    refMyVideo.current.srcObject = stream
                })
        }
        getMedia();
    }, [])

    return (
        <div className="content d-flex flex-column flex-column-fluid" id="kt_content">
            <div className="d-flex flex-column-fluid">
                <div className="container-fluid">

                    <div className="row">
                        <div className="col-md-12">
                            <div className="card card-custom card-stretch gutter-b">
                                <div className="card-body pt-2 pb-0 mt-n3">
                                    <video name="video" controls playsInline ref={refMyVideo}></video>

                                    {/* <video width="300" height="300" controls >
                                        <source src={wav} type="video/mp4" />
                                    </video>
                                    <audio  controls >
                                        <source src={wav} type="audio/wav" />
                                    </audio> */}
                                    <a href="https://pabx.keurais.com:8887/api/files/676473928905916416/data">wav</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default MediaStream
