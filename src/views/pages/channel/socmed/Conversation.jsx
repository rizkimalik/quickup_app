import React from 'react'
import Attachment from './Attachment';
import { DatetimeFormat } from 'views/components/Datetime';

function Conversation({ conversation, selected_customer, refContentChat }) {
    return (
        <div ref={refContentChat} style={{ height: 'calc(75vh - 130px)', overflowY: 'auto' }}>
            <div className="messages p-8">
                {
                    // conversation?.map((item, index) => {
                    //? remove duplicate array 
                    [...new Set(conversation)]?.filter((item) => item.chat_id === selected_customer.chat_id).sort((a, b) => a.id - b.id).map((item, index) => {
                        if (item.flag_to === 'customer') {
                            return (
                                <div className="d-flex justify-content-start mb-10" key={index}>
                                    <div className="d-flex flex-column align-items-start">
                                        <div className="d-flex align-items-center mb-2">
                                            <span className="fs-5 fw-bolder text-gray-900 text-hover-primary">{item.name}</span>
                                        </div>
                                        <div className="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">
                                            {item.attachment && <Attachment value={item.attachment} />}
                                            {item.message}
                                        </div>
                                        <div className="d-flex flex-column align-items-end mt-1">
                                            <div className="d-flex align-items-center justify-content-between">
                                                <span className="label label-sm label-rounded label-secondary" title={item.flag_notif === 1 ? 'Read' : 'Unread'}>
                                                    {
                                                        item.flag_notif === 1
                                                            ? <i className="fas fa-check-double icon-xs m-1 text-success" />
                                                            : <i className="fas fa-check icon-xs m-1" />
                                                    }
                                                </span>
                                                <label className="text-muted font-size-xs m-1">{DatetimeFormat(item.date_create)}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        else if (item.flag_to === 'agent') {
                            return (
                                <div className="d-flex justify-content-end mb-10" key={index}>
                                    <div className="d-flex flex-column align-items-end">
                                        <div className="d-flex align-items-center mb-2">
                                            <span className="fs-5 fw-bolder text-gray-900 text-hover-primary ms-1">{item.name}</span>
                                        </div>
                                        <div className="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text">
                                            {item.attachment && <Attachment value={item.attachment} />}
                                            {item.message}
                                        </div>
                                        <div className="d-flex flex-column align-items-end mt-1">
                                            <div className="d-flex align-items-center justify-content-between">
                                                <label className="text-muted font-size-xs m-1">{DatetimeFormat(item.date_create)}</label>
                                                <span className="label label-sm label-rounded label-secondary" title={item.flag_sent === 1 ? 'Sent' : item.flag_sent === 2 ? 'Failed' : 'Waiting'}>
                                                    {
                                                        item.flag_sent === 1 ? <i className="fas fa-check-double icon-xs m-1 text-success" />
                                                            : item.flag_sent === 2 ? <i className="fas fa-exclamation-circle icon-xs m-1 text-danger" />
                                                                : <i className="fas fa-clock icon-xs m-1" />
                                                    }
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        }

                        return ('');
                    })
                }
            </div>
        </div>
    )
}

export default Conversation