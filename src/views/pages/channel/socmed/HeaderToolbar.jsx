import React, { useState } from 'react'
import { Link, NavLink } from 'react-router-dom';

import { TransferAssignedAgent } from '.';
import { IconUserGroup, IconSetting, IconMark } from 'views/components/icon'
import CustomerJourney from 'views/pages/customer/CustomerJourney';

function HeaderToolbar({ handlerEndChat, selected_customer, handlerAccountSpam }) {
    const [data_assign, setDataAssign] = useState('');
    const [navigate, setNavigate] = useState('');

    return (
        <div>
            {data_assign.action === 'social_media_assign' && <TransferAssignedAgent values={data_assign} />}
            {
                navigate === 'Journey' &&
                selected_customer.customer_id &&
                <CustomerJourney customer={selected_customer} />
            }

            <button type="button" className="btn btn-icon btn-sm btn-light-danger btn-circle ml-2" title="End Chat" onClick={(e) => handlerEndChat(selected_customer)}>
                <i className="far fa-window-close fa-sm"></i>
            </button>
            <button className="btn btn-sm btn-light-primary btn-icon btn-circle ml-3" data-toggle="dropdown" aria-expanded="false">
                <IconSetting className="svg-icon svg-icon-md" />
            </button>
            <div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <ul className="navi flex-column navi-hover py-2">
                    <li className="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Choose an action: </li>
                    <li className="navi-item">
                        <NavLink to={`/customer/${selected_customer.customer_id}/edit`} className="navi-link">
                            <IconUserGroup className="navi-icon" /> Detail Customer
                        </NavLink>
                    </li>
                    <li className="navi-item">
                        <Link to="#" onClick={() => setDataAssign({ action: 'social_media_assign', current_agent: selected_customer.agent_handle, interaction_id: selected_customer.chat_id })} data-toggle="modal" data-target="#modalTransferAssignedAgent" className="navi-link">
                            <IconMark className="navi-icon" /> Assigned Agent
                        </Link>
                    </li>
                    <li className="navi-item">
                        <Link to="#" onClick={() => setNavigate('Journey')} className="navi-link" data-toggle="modal" data-target="#modalJourneyCustomer">
                            <i className="fas fa-route fa-lg mr-2"></i> Customer Journey
                        </Link>
                    </li>
                    <li className="navi-item">
                        <div className="dropdown-divider"></div>
                    </li>
                    <li className="navi-item">
                        <Link to="#" onClick={(e) => handlerAccountSpam(selected_customer)} className="navi-link">
                            <i className="far fa-eye-slash fa-lg mr-2"></i> Block Chat
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default HeaderToolbar