import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { IconMark } from 'views/components/icon';
import { authUser } from 'app/slice/sliceAuth';
import { apiAgentReadyAssign, apiTransferAssignAgent } from 'app/services/apiSosmed';
import { getEmailInboxData } from 'app/services/apiEmail';
import { ButtonRefresh } from 'views/components/button';
import { getListCustomer } from 'app/services/apiSosmed';
import { socket } from 'app/config';

function TransferAssignedAgent({ values }) {
    const dispatch = useDispatch();
    const user = useSelector(authUser);
    const { agent_ready_toassign } = useSelector(state => state.sosialmedia);
    const [fields, setFields] = useState('');

    useEffect(() => {
        dispatch(apiAgentReadyAssign())
    }, [dispatch]);

    function componentButtonActions(data) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    onClick={(e) => setFields(data)}
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> Select Agent
                </button>
            </div>
        )
    }

    const submitTransferAssignAgent = async () => {
        if (values.current_agent === fields.username) {
            SwalAlertError('Assign Failed.', 'Cannot assign data same Agent.');
        }
        else {
            const { payload } = await dispatch(apiTransferAssignAgent({
                interaction_id: values.interaction_id,
                agent_handle: fields.username,
                channel: values.action === 'email_assign' ? 'Email' : 'SosialMedia'
            }))

            if (payload.status === 200) {
                socket.emit('transfer-assign-agent', {
                    agent_handle: fields.username,
                    name: user.username,
                    chat_id: values.interaction_id,
                    channel: values.action === 'email_assign' ? 'Email' : 'SosialMedia',
                    message: 'Receive Assigned Agent.'
                });
                if (values.action === 'email_assign') {
                    dispatch(getEmailInboxData({ agent_handle: user.username, user_level: user.user_level })); // refresh data email inbox
                }
                else if (values.action === 'social_media_assign') {
                    dispatch(getListCustomer({ agent_handle: user.username })); //refresh data list cust sosial media
                }
                setFields('');
                SwalAlertSuccess('Succsessfuly', payload.data[0].message);
            }
            else {
                SwalAlertError('Failed.', payload.data[0].message);
            }
        }
    }

    return (
        <Modal id="modalTransferAssignedAgent">
            <ModalHeader title={`Transfer Assigned Agent - ID#${values.interaction_id}`} />
            <ModalBody>
                <div className="list border rounded mb-4">
                    <div className='list-item d-flex align-items-center justify-content-between'>
                        <div className="d-flex align-items-center py-2 mx-2">
                            <span className="bullet bullet-bar bg-primary align-self-stretch mr-2"></span>
                            <div className="symbol symbol-40px">
                                <div className="symbol-label">
                                    <i className="fas fa-user text-primary"></i>
                                </div>
                            </div>
                            <div className="flex-grow-1 mx-2">
                                <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">Current Agent</div>
                                <div className="mt-2">
                                    <span className="text-mute m-2">{values.current_agent}</span>
                                </div>
                            </div>
                            <div className="flex-grow-1 mx-2">
                                <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">Transfer To</div>
                                <div className="mt-2">
                                    <span className="text-mute m-2">
                                        <span className="svg-icon svg-icon-primary svg-icon-sm">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fillRule="nonzero" />
                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fillRule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div className="flex-grow-1 mx-2">
                                <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">Handle Agent</div>
                                <div className="mt-2">
                                    <span className="text-mute m-2">{fields.username}</span>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex flex-column align-items-end mx-2">
                            <button type="button" onClick={() => submitTransferAssignAgent()} className="btn btn-primary font-weight-bold">Send Assigned Agent</button>
                        </div>
                    </div>
                </div>

                <DataGrid
                    dataSource={agent_ready_toassign}
                    remoteOperations={true}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                    columnMinWidth={120}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Action" dataField="id" cellRender={({ data }) => componentButtonActions(data)} fixed={true} />
                    <Column caption="Username" dataField="username" fixed={true} />
                    <Column caption="Name" dataField="name" />
                    <Column caption="Email" dataField="email_address" />
                    <Column caption="Department" dataField="department_name" />
                    <Column caption="Channel Handle">
                        <Column caption="Email" dataField="email" cellRender={({ value }) => {
                            return <span>{value ? 'Yes' : 'No'}</span>
                        }} />
                        <Column caption="Whatsapp" dataField="whatsapp" cellRender={({ value }) => {
                            return <span>{value ? 'Yes' : 'No'}</span>
                        }} />
                        <Column caption="Facebook" dataField="facebook" cellRender={({ value }) => {
                            return <span>{value ? 'Yes' : 'No'}</span>
                        }} />
                        <Column caption="Instagram" dataField="instagram" cellRender={({ value }) => {
                            return <span>{value ? 'Yes' : 'No'}</span>
                        }} />
                        <Column caption="Twitter" dataField="twitter" cellRender={({ value }) => {
                            return <span>{value ? 'Yes' : 'No'}</span>
                        }} />
                        <Column caption="Chat" dataField="chat" cellRender={({ value }) => {
                            return <span>{value ? 'Yes' : 'No'}</span>
                        }} />
                    </Column>
                </DataGrid>
            </ModalBody>
            <ModalFooter>
                <ButtonRefresh onClick={() => dispatch(apiAgentReadyAssign())} />
            </ModalFooter>
        </Modal>
    )
}

export default TransferAssignedAgent