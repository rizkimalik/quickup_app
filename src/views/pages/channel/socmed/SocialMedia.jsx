import React, { useState, useEffect, useRef } from 'react';
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, useHistory } from 'react-router-dom';

import RandomString from 'views/components/RandomString';
import SplashScreen from 'views/components/SplashScreen';
import { socket, baseUrl } from 'app/config';
import { Datetime } from 'views/components/Datetime';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { authUser } from 'app/slice/sliceAuth';
import { getListCustomer, getLoadConversation, getEndChat, setAccountSpam } from 'app/services/apiSosmed';
import { setSelectedCustomer } from 'app/slice/sliceSosmed';
import { ListCustomer, Conversation, FormSendMessage, HeaderToolbar } from '.';
import { setTicketThread } from 'app/slice/sliceTicket';
import { IconClock, IconLayoutPanel } from 'views/components/icon';

const SocialMedia = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const refContentChat = useRef();
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState('');
    const [conversation, setConversation] = useState([]);
    const [page, setPage] = useState(0);
    const [attachment, setAttachment] = useState('');
    const [scroll_top, setBtnScrollTop] = useState(false);
    const [typing, setTyping] = useState(false);
    const [load_customer, setLoadCustomer] = useState(false);
    const { username, user_level } = useSelector(authUser);
    const { selected_customer, conversations } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        dispatch(getListCustomer({ agent_handle: username }))
    }, [dispatch, username, load_customer]);

    useEffect(() => {
        function showMessageContent(res) {
            setLoadCustomer(true)
            if (selected_customer) {
                if (res.chat_id === selected_customer.chat_id) {
                    setConversation(conversation => [...conversation, res]);
                    refContentChat.current.scrollTo({ bottom: 0, behavior: 'smooth' }, refContentChat.current.scrollHeight); //? auto scroll to down
                }
            }
        }

        socket.on('return-typing', (res) => {
            setTyping(res.typing);
        });
        socket.on('send-message-customer', (res) => { //push from blending
            showMessageContent(res);
        });
        socket.on('return-message-customer', (res) => {
            showMessageContent(res);
        });
        socket.on('return-message-whatsapp', (res) => {
            showMessageContent(res);
        });
        socket.on('return-directmessage-twitter', (res) => {
            showMessageContent(res);
        });
        socket.on('return-facebook-messenger', (res) => {
            showMessageContent(res);
        });
        socket.on('return-instagram-messenger', (res) => {
            showMessageContent(res);
        });
        socket.on('return-message-telegram', (res) => {
            showMessageContent(res);
        });
        socket.on('return-transfer-assign-agent', (res) => {
            showMessageContent(res);
        });
        setLoadCustomer(false)
    }, [selected_customer]);

    useEffect(() => {
        if (conversations.data) {
            setConversation(conversation => [...conversations.data, ...conversation]);
        }
    }, [page, conversations]);

    function sendMessage(e) {
        e.preventDefault();

        let attachment_file = '';
        let message_type = 'text';
        if (attachment) {
            // let extention = '.' + (attachment.name).split('.').pop();
            let extention = (attachment.type).split('/')[0] === 'image' ? '.jpg' : '.' + (attachment.name).split('.').pop();
            let generate_filename = RandomString(20) + extention;
            message_type = (attachment.type).split('/')[0] === 'image' ? 'image' : (attachment.type).split('/')[0] === 'video' ? 'video' : 'document';
            attachment_file = [{
                attachment: attachment,
                file_origin: attachment?.name,
                file_name: generate_filename,
                file_size: attachment?.size,
                file_type: message_type,
                file_url: attachment ? `attachment/${selected_customer.channel}/${generate_filename}` : '',
                file_base: baseUrl,
            }];
        }

        let values = {
            username: selected_customer.agent_handle,
            chat_id: selected_customer.chat_id,
            user_id: selected_customer.user_id,
            customer_id: selected_customer.customer_id,
            message: message,
            message_type: message_type,
            name: username,
            email: selected_customer.email,
            channel: selected_customer.channel,
            flag_to: 'agent',
            agent_handle: username,
            page_id: selected_customer.page_id,
            date_create: Datetime(),
            uuid_customer: socket.id,
            uuid_agent: selected_customer.uuid_agent,
            attachment: attachment_file,
            typing: false
        }

        if (message || attachment) {
            if (values.channel === 'Chat') {
                socket.emit('send-message-agent', values);
            }
            else if ((values.channel).split('_')[0] === 'Whatsapp') {
                socket.emit('send-message-whatsapp', values);
            }
            else if (values.channel === 'Twitter_DM') {
                socket.emit('send-directmessage-twitter', values);
            }
            else if (values.channel === 'Facebook_Messenger') {
                socket.emit('send-facebook-messenger', values);
            }
            else if (values.channel === 'Instagram_Messenger') {
                socket.emit('send-instagram-messenger', values);
            }
            else if (values.channel === 'Telegram') {
                socket.emit('send-message-telegram', values);
            }

            setConversation(conversation => [...conversation, values]);
            setTimeout(() => {
                refContentChat.current.scrollTo({ bottom: 0, behavior: 'smooth' }, refContentChat.current.scrollHeight); //? auto scroll to down
                dispatch(getListCustomer({ agent_handle: username }))
            }, 500);
        }
        setMessage('');
        setAttachment('');
    }

    function onKeyTyping() {
        let data = {
            uuid_customer: socket.id,
            uuid_agent: selected_customer.uuid_agent,
            email: selected_customer.email,
            agent_handle: selected_customer.agent_handle,
            flag_to: 'agent',
            typing: true
        }
        socket.emit('typing', data);
    }

    function handlerSelectCustomer(customer) {
        socket.emit('join-room', customer.chat_id);
        setPage(0);
        setLoading(true);
        setBtnScrollTop(false)
        setConversation([]);
        dispatch(setSelectedCustomer(customer))
        dispatch(getListCustomer({ agent_handle: username }))
        setTimeout(() => {
            dispatch(getLoadConversation({ skip: 0, take: 10, chat_id: customer.chat_id, customer_id: customer.customer_id }));
        }, 500);
        setTimeout(() => {
            refContentChat.current.scrollTo({ bottom: 0, behavior: 'smooth' }, refContentChat.current.scrollHeight); //? auto scroll to down
            setLoading(false);
        }, 1000);
    }

    function handlerEndChat(value) {
        const { chat_id, customer_id, user_id, channel } = value;
        Swal.fire({
            title: 'End Session',
            text: 'Do you want to close the conversations?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'End Session',
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: 'Add to Ticket',
                    text: 'open form create ticket now.',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Create Ticket',
                    cancelButtonText: 'No',
                }).then((res) => {
                    if (res.isConfirmed) {
                        dispatch(getEndChat({ chat_id, customer_id }));
                        dispatch(setSelectedCustomer({}));
                        setConversation([]);
                        history.push(`/ticket?thread_id=${chat_id}&channel=${channel}&account=${user_id}`);
                    }
                    else {
                        dispatch(setTicketThread({
                            thread_id: chat_id,
                            thread_channel: channel,
                            account: user_id,
                            subject: 'interaction data sosmed.'
                        }));
                        dispatch(getEndChat({ chat_id, customer_id }));
                        dispatch(setSelectedCustomer({}));
                        setConversation([]);
                        dispatch(getListCustomer({ agent_handle: username }));
                    }
                })
            }
        })
    }

    function handlerAccountSpam(value) {
        Swal.fire({
            title: 'Spam Chat.',
            text: 'Do you want to report the chat?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Report',
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(setAccountSpam(value));
                dispatch(getListCustomer({ agent_handle: username }))
                dispatch(setSelectedCustomer({}))
                dispatch(getLoadConversation({ skip: 0, take: 10, chat_id: value.chat_id, customer_id: value.customer_id }))
            }
        })
    }

    const handlerScrollToTop = async () => {
        if (refContentChat.current.scrollTop === 0 && !loading) {
            setLoading(true);
            try {
                await dispatch(getLoadConversation({ skip: page + 10, take: 10, chat_id: selected_customer.chat_id, customer_id: selected_customer.customer_id }))
                setPage(page + 10);
            } catch (error) {
                console.error('Error fetching data: ', error);
            } finally {
                setLoading(false);
                refContentChat.current.scrollTo({ top: 0, behavior: 'smooth' });
            }
        }
    }

    useEffect(() => {
        function getScrollTop() {
            refContentChat.current.scrollTop === 0 ? setBtnScrollTop(true) : setBtnScrollTop(false);
        }
        refContentChat.current.addEventListener('scroll', getScrollTop);
        return () => refContentChat.current.removeEventListener('scroll', getScrollTop);
    }, []);

    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Social Media" modul_name="Live Messages">
                <NavLink to="/omnichannel/feed" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                    <IconLayoutPanel className="svg-icon svg-icon-sm" /> Feed & Comments
                </NavLink>
                <NavLink to="/omnichannel/history" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                    <IconClock className="svg-icon svg-icon-sm" /> History Social Media
                </NavLink>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-4">
                        <ListCustomer handlerSelectCustomer={handlerSelectCustomer} getListCustomer={getListCustomer} />
                    </div>
                    <div className="col-lg-8">
                        {loading && <SplashScreen />}
                        <Card>
                            <CardHeader className="border-bottom">
                                <CardTitle
                                    title={selected_customer?.name}
                                    subtitle={selected_customer?.user_id && typing ? 'Typing..' : selected_customer?.user_id}
                                />
                                {
                                    (user_level === 'L1' || user_level === 'SPV') &&
                                    selected_customer?.chat_id &&
                                    <CardToolbar>
                                        <HeaderToolbar
                                            handlerEndChat={handlerEndChat}
                                            selected_customer={selected_customer}
                                            handlerAccountSpam={handlerAccountSpam}
                                        />
                                    </CardToolbar>
                                }
                            </CardHeader>
                            <CardBody className="p-0">
                                {
                                    scroll_top === true &&
                                    <div className="d-flex justify-content-center">
                                        <button onClick={handlerScrollToTop} className="btn btn-outline-primary btn-sm btn-pill mt-4" title="Loaded more Messages.">
                                            <i className="fas fa-arrow-up"></i> Load Messages
                                        </button>
                                    </div>
                                }
                                <Conversation
                                    conversation={conversation}
                                    selected_customer={selected_customer}
                                    refContentChat={refContentChat}
                                />
                            </CardBody>

                            {
                                (user_level === 'L1' || user_level === 'SPV') &&
                                selected_customer.customer_id &&
                                <FormSendMessage
                                    message={message}
                                    setMessage={setMessage}
                                    attachment={attachment}
                                    setAttachment={setAttachment}
                                    sendMessage={sendMessage}
                                    onKeyTyping={onKeyTyping}
                                />
                            }
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default SocialMedia