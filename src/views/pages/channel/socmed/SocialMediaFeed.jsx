import React from 'react'
// import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { NavLink, useHistory } from 'react-router-dom';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { ListCustomer } from '.';
import { IconLayoutPanel } from 'views/components/icon';
import Icons from 'views/components/Icons';

function SocialMediaFeed() {

    function handlerSelectCustomer(customer) {

    }

    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Social Media" modul_name="Feed & Comments">
                <NavLink to="/omnichannel/socmed" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                    Live Messages
                </NavLink>
                <NavLink to="/omnichannel/history" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                    History Social Media
                </NavLink>
            </SubHeader>
            <Container>
                <div className="d-flex flex-row">
                    <ListCustomer handlerSelectCustomer={handlerSelectCustomer} />
                    <div className="flex-row-fluid ml-lg-4">
                        <div className="card card-custom gutter-b border">
                            <div className="card-body">
                                <div className="d-flex align-items-center">
                                    <div className="symbol symbol-45 symbol-light mr-5">
                                        <span className="symbol-label">
                                            <IconLayoutPanel className="svg-icon svg-icon-lg svg-icon-primary" />
                                        </span>
                                    </div>
                                    <div className="d-flex flex-column flex-grow-1">
                                        <a href="#" className="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">title feed WWII Quiz Pt 14</a>
                                        <div className="d-flex">
                                            <div className="d-flex align-items-center pr-5">
                                                <span className="svg-icon svg-icon-md svg-icon-primary pr-1">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                                            <rect x={0} y={0} width={24} height={24} />
                                                            <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" fill="#000000" opacity="0.3" />
                                                            <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000" />
                                                        </g>
                                                    </svg>
                                                </span>
                                                <span className="text-muted font-weight-bold">Due 04 Sep</span>
                                            </div>
                                            <div className="d-flex align-items-center">
                                                <span className="svg-icon svg-icon-md svg-icon-primary pr-1">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                                            <rect x={0} y={0} width={24} height={24} />
                                                            <path d="M5.5,4 L9.5,4 C10.3284271,4 11,4.67157288 11,5.5 L11,6.5 C11,7.32842712 10.3284271,8 9.5,8 L5.5,8 C4.67157288,8 4,7.32842712 4,6.5 L4,5.5 C4,4.67157288 4.67157288,4 5.5,4 Z M14.5,16 L18.5,16 C19.3284271,16 20,16.6715729 20,17.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,17.5 C13,16.6715729 13.6715729,16 14.5,16 Z" fill="#000000" />
                                                            <path d="M5.5,10 L9.5,10 C10.3284271,10 11,10.6715729 11,11.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,12.5 C20,13.3284271 19.3284271,14 18.5,14 L14.5,14 C13.6715729,14 13,13.3284271 13,12.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z" fill="#000000" opacity="0.3" />
                                                        </g>
                                                    </svg>{/*end::Svg Icon*/}</span>
                                                <span className="text-muted font-weight-bold">Keurais Desk Fan Page</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="pt-4">
                                    <div className="bgi-no-repeat bgi-size-cover rounded min-h-295px">
                                        <a href="https://scontent-cgk1-2.cdninstagram.com/v/t51.29350-15/373698406_283291111079176_1153739281412699139_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=8ae9d6&_nc_ohc=SHoxafVe4nEAX9iHjuf&_nc_ht=scontent-cgk1-2.cdninstagram.com&edm=AEQ6tj4EAAAA&oh=00_AfBAp2uOBaa_IuBUfrBSbi1zHVG8PioA0F_LojNAcsCOyg&oe=64FADA56" target="_blank" rel="noopener noreferrer" title="Click to Show">
                                            <img src="https://scontent-cgk1-2.cdninstagram.com/v/t51.29350-15/373698406_283291111079176_1153739281412699139_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=8ae9d6&_nc_ohc=SHoxafVe4nEAX9iHjuf&_nc_ht=scontent-cgk1-2.cdninstagram.com&edm=AEQ6tj4EAAAA&oh=00_AfBAp2uOBaa_IuBUfrBSbi1zHVG8PioA0F_LojNAcsCOyg&oe=64FADA56" height={145} className="border" alt="" />
                                        </a>
                                    </div>
                                    <p className="text-dark-75 font-size-lg font-weight-normal pt-4">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors
                                    </p>
                                    <div className="pt-4">
                                        <button type="button" className="btn btn-light-primary btn-sm rounded font-weight-bolder font-size-sm p-2">
                                            <Icons iconName="group-chat" className="svg-icon svg-icon-md pr-2" /> 24 Comments
                                        </button>
                                    </div>
                                    <div className="separator separator-solid mt-4" />

                                    <div style={{ height: '400px', overflowY: 'auto' }}>
                                        <div className="d-flex pt-4">
                                            <div className="symbol symbol-40 symbol-light-success mr-5 mt-1">
                                                <span className="symbol-label">A</span>
                                            </div>
                                            <div className="d-flex flex-column flex-row-fluid">
                                                <div className="d-flex align-items-center flex-wrap">
                                                    <a href="#" className="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder pr-6">Mr. Anderson</a>
                                                    <span className="text-muted font-weight-normal flex-grow-1 font-size-sm">1 Day ago</span>
                                                    <span className="text-muted font-weight-normal font-size-sm">Reply</span>
                                                </div>
                                                <span className="text-dark-75 font-size-sm font-weight-normal pt-1">
                                                    Long before you sit dow to put digital pen to
                                                    paper you need to make sure you have to sit down and write.
                                                </span>

                                                <div className="d-flex pt-4">
                                                    <div className="symbol symbol-40 symbol-light-success mr-5 mt-1">
                                                        <span className="symbol-label">A</span>
                                                    </div>
                                                    <div className="d-flex flex-column flex-row-fluid">
                                                        <div className="d-flex align-items-center flex-wrap">
                                                            <a href="#" className="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder pr-6">Mrs. Anderson</a>
                                                            <span className="text-muted font-weight-normal flex-grow-1 font-size-sm">2 Days ago</span>
                                                            <span className="text-muted font-weight-normal font-size-sm">Reply</span>
                                                        </div>
                                                        <span className="text-dark-75 font-size-sm font-weight-normal pt-1">
                                                            Long before you sit down to put digital pen to paper
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* <div className="separator separator-solid mt-9 mb-4" />
                                <form className="position-relative">
                                    <textarea id="kt_forms_widget_11_input" className="form-control border-0 p-0 pr-10 resize-none" rows={1} placeholder="Any Question?" style={{ overflow: 'hidden', overflowWrap: 'break-word', height: 20 }} defaultValue={""} />
                                    <div className="position-absolute top-0 right-0 mt-n1 mr-n2">
                                        <span className="btn btn-icon btn-sm btn-hover-icon-primary">
                                            <i className="flaticon2-clip-symbol icon-ms" />
                                        </span>
                                    </div>
                                </form> */}
                            </div>
                        </div>

                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default SocialMediaFeed