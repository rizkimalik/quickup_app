import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { IconGroupChat, IconLayoutPanel, IconMention, IconMailHistory, IconCall } from 'views/components/icon';
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import MessagesDataHistory from './MessagesDataHistory';

function SosialMediaHistory() {
    const [navigate, setNavigate] = useState('Messages');

    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Social Media" modul_name="Data History">
                <NavLink to="/omnichannel/call" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconCall className="svg-icon svg-icon-sm" /> Data Call
                </NavLink>
                <NavLink to="/omnichannel/email" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconMailHistory className="svg-icon svg-icon-sm" /> Data Mailbox
                </NavLink>
                <NavLink to="/omnichannel/history" className="btn btn-light font-weight-bolder btn-sm m-1">
                    <IconGroupChat className="svg-icon svg-icon-sm" /> Data Sosial Media
                </NavLink>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="card card-custom gutter-b card-stretch card-shadowless">
                            <div className="card-body p-0">
                                <ul className="dashboard-tabs nav nav-pills nav-light-primary row row-paddingless m-0 p-0 flex-column flex-sm-row" role="tablist">
                                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                                        <a href="#tab_all_messages" onClick={(e) => setNavigate('Messages')} className={`nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center ${navigate === 'Messages' ?? 'active'}`} data-toggle="pill">
                                            <div className="d-flex flex-row">
                                                <span className="nav-icon py-2 w-auto">
                                                    <IconGroupChat className="svg-icon svg-icon-3x" />
                                                </span>
                                            </div>
                                            <span className="nav-text font-size-lg py-2 font-weight-bold text-center">
                                                All Messages
                                            </span>
                                        </a>
                                    </li>
                                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                                        <a href="#tab_mentions" onClick={(e) => setNavigate('Mentions')} className={`nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center ${navigate === 'Mentions' ?? 'active'}`} data-toggle="pill">
                                            <div className="d-flex flex-row">
                                                <span className="nav-icon py-2 w-auto">
                                                    <IconMention className="svg-icon svg-icon-3x" />
                                                </span>
                                            </div>
                                            <span className="nav-text font-size-lg py-2 font-weight-bolder text-center">
                                                Mentions
                                            </span>
                                        </a>
                                    </li>
                                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                                        <a href="#tab_feeds" onClick={(e) => setNavigate('Feeds')} className={`nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center ${navigate === 'Feeds' ?? 'active'}`} data-toggle="pill">
                                            <div className="d-flex flex-row">
                                                <span className="nav-icon py-2 w-auto">
                                                    <IconLayoutPanel className="svg-icon svg-icon-3x" />
                                                </span>
                                            </div>
                                            <span className="nav-text font-size-lg py-2 font-weight-bolder text-center">
                                                Feed & Comments
                                            </span>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="card card-custom gutter-b border py-6">
                            <div className="card-body d-flex align-items-center justify-content-between flex-wrap">
                                <div className="mr-2">
                                    <h3 className="font-weight-bolder">Sosial Media</h3>
                                    <div className="text-dark-50 font-size-md mt-2">
                                        Communication with chat, whatsapp, facebook, twitter, instagram.
                                    </div>
                                </div>
                                <NavLink to="/omnichannel/socmed" className="btn btn-primary font-weight-bold py-3 px-6">Live Messages</NavLink>
                            </div>
                        </div>

                    </div>
                </div>


                <div className="row">
                    <div className="col-lg-12">
                        <div className="tab-content m-0 p-0">
                            <div className="tab-pane active" id="tab_all_messages" role="tabpanel">
                                {navigate === 'Messages' && <MessagesDataHistory />}
                            </div>
                            <div className="tab-pane" id="tab_mentions" role="tabpanel">

                            </div>
                            <div className="tab-pane" id="tab_feeds" role="tabpanel">

                            </div>
                        </div>

                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default SosialMediaHistory