import React, { useEffect, useRef, useState } from 'react'
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { apiChatTemplate } from 'app/services/apiSosmed';

function FormSendMessage({ message, setMessage, sendMessage, onKeyTyping, attachment, setAttachment }) {
    const inputFileRef = useRef(null);
    const dispatch = useDispatch();
    const [templates, setTemplates] = useState([]);
    const { chat_templates } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        setTemplates(chat_templates);
    }, [chat_templates]);

    const onFileChange = (e) => {
        console.log(e.target.files[0]);
        if (e.target.files[0].size > 3000000) {
            alert('Max size file 3mb.');
        } else {
            setAttachment(e.target.files[0]);
        }
        e.target.value = "";
    }

    const onFileClose = (e) => {
        setAttachment('')
        e.target.value = "";
    }

    const onFilterTemplates = (value) => {
        // filter by regex like 'value' short_code data
        const results = chat_templates.filter((template) => {
            const regex = new RegExp(value, 'i');
            return regex.test(template.short_code);
        });
        setTemplates(results);
    }

    return (
        <div className="card-footer bg-light p-0">
            {
                attachment &&
                <div className="alert alert-custom alert-notice alert-light-primary fade show p-2" role="alert">
                    <div className="alert-icon"><i className="fa fa-file-image" /></div>
                    <div className="alert-text">{attachment?.name}</div>
                    <div className="alert-close">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={onFileClose}>
                            <span aria-hidden="true"><i className="ki ki-close" /></span>
                        </button>
                    </div>
                </div>
            }
            <div className="form-group m-4">
                <div className="input-group">
                    <div className="input-group-prepend">
                        <button onClick={() => dispatch(apiChatTemplate())} className="btn btn-secondary" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Message Template">
                            <i className="fas fa-comment-alt" />
                        </button>
                        <div className="dropdown-menu dropdown-menu-lg">
                            <div className="px-4">
                                <div className="input-group">
                                    <input type="text" onKeyUp={(e) => onFilterTemplates(e.target.value)} className="form-control form-control-sm" placeholder="Search.." />
                                    <div className="input-group-append">
                                        <NavLink to="/master/chat_template" className="btn btn-light-primary btn-sm">Add Template</NavLink>
                                    </div>
                                </div>
                            </div>
                            <div className="dropdown-divider" />
                            <div className="px-1" style={{ overflowX: 'auto', height: 350 }}>
                                {
                                    // chat_templates?.map((item, index) => {
                                    templates?.map((item, index) => {
                                        return <button key={index} onClick={() => setMessage(item.content_message)} className="dropdown-item text-wrap">
                                            <span className="font-weight-bolder text-dark">{item.short_code}</span> -  {item.content_message}
                                        </button>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className="input-group-prepend">
                        <input type="file" ref={inputFileRef} onChange={onFileChange} accept="image/*, .doc, .pdf, .xls, .xlsx, .mp4" className="hide" />
                        <button type="button" onClick={() => inputFileRef.current.click()} title="Attachment Files" className="btn btn-secondary"><i className="fa fa-paperclip" /></button>
                    </div>
                    <input style={{ zIndex: 1000 }}
                        type="text"
                        name="message"
                        className="form-control"
                        placeholder="Type a message"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                        onKeyPress={(e) => e.key === 'Enter' ? sendMessage(e) : onKeyTyping()}
                    />
                    <div className="input-group-append">
                        <button onClick={sendMessage} className="btn btn-secondary" type="button"><i className="fa fa-paper-plane" /> Send</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FormSendMessage