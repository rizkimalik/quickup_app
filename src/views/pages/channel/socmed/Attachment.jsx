import React from 'react'
import { baseUrl } from 'app/config'

function Attachment({ value }) {
    return (
        <div className="d-flex align-items-center">
            {
                value?.map((item, index) => {
                    return (
                        <div className="border border-dashed border-primary rounded p-2 mr-2" key={index}>
                            {/* // <iframe width="100%" height="100%" src={baseUrl + '/' + item.file_url} title={item.file_name} frameBorder={0} allowFullScreen allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" /> */}
                            <div className="d-flex justify-content-center">
                                {
                                    item.file_type === 'image' ?
                                        <img width="auto" height="180px" src={baseUrl + '/' + item.file_url} alt={item.file_name} />
                                        : item.file_type === 'video' ?
                                            <video width="auto" height="180px" src={baseUrl + '/' + item.file_url} alt={item.file_name} controls /> : ''
                                }
                            </div>
                            <div className="d-flex flex-aligns-center mt-2">
                                <div className="flex-shrink-0 avatar-sm mx-2 ms-0 attached-file-avatar">
                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">
                                        {
                                            item.file_type === 'document'
                                                ? <i className="fa fa-paperclip align-middle" />
                                                : <i className="fa fa-file-image align-middle" />
                                        }
                                    </div>
                                </div>
                                <div className="ms-1 fw-semibold">
                                    <a href={baseUrl + '/' + item.file_url} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold">{item.file_name}</a>
                                    {
                                        item.file_size > 0 &&
                                        <div className="text-gray-400">{item.file_size / 1000} kb</div>
                                    }
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default Attachment