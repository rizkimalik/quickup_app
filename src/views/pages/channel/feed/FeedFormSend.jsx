import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { apiChatTemplate } from 'app/services/apiSosmed';

function FeedFormSend({ message, setMessage, sendReplyMessage }) {
    const dispatch = useDispatch();
    const [templates, setTemplates] = useState([]);
    const { chat_templates } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        setTemplates(chat_templates);
    }, [chat_templates]);

    const onFilterTemplates = (value) => {
        // filter by regex like 'value' short_code data
        const results = chat_templates.filter((template) => {
            const regex = new RegExp(value, 'i');
            return regex.test(template.short_code);
        });
        setTemplates(results);
    }

    return (
        <div>
            <div className="form-group my-4">
                {/* <div className="input-group"> */}
                <div className="d-flex flex-column">
                    <textarea
                        className="form-control"
                        name="message"
                        rows="2"
                        placeholder="Type a reply..."
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                    // onKeyPress={(e) => e.key === 'Enter' ? sendReplyMessage(e) : ''}
                    ></textarea>
                    <div className="input-group-append mt-2">
                        <button type="button" onClick={sendReplyMessage} className="btn btn-sm btn-secondary btn-hover-primary mr-2">Send</button>
                        <button onClick={() => dispatch(apiChatTemplate())} className="btn btn-icon btn-sm btn-secondary btn-hover-primary" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Message Template">
                            <i className="fas fa-comment-alt" />
                        </button>
                        <div className="dropdown-menu dropdown-menu-lg">
                            <div className="px-4">
                                <div className="input-group">
                                    <input type="text" onKeyUp={(e) => onFilterTemplates(e.target.value)} className="form-control form-control-sm" placeholder="Search.." />
                                    <div className="input-group-append">
                                        <NavLink to="/master/chat_template" className="btn btn-light-primary btn-sm">Add Template</NavLink>
                                    </div>
                                </div>
                            </div>
                            <div className="dropdown-divider" />
                            <div className="px-1" style={{ overflowX: 'auto', height: 350 }}>
                                {
                                    // chat_templates?.map((item, index) => {
                                    templates?.map((item, index) => {
                                        return <button key={index} onClick={() => setMessage(item.content_message)} className="dropdown-item text-wrap">
                                            <span className="font-weight-bolder text-dark">{item.short_code}</span> -  {item.content_message}
                                        </button>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FeedFormSend