import React, { useState } from 'react'
import { Link, NavLink } from 'react-router-dom';

import CustomerJourney from 'views/pages/customer/CustomerJourney';
import { TransferAssignedAgent } from '../socmed';
import { IconLayoutPanel, IconClock, IconMark, IconUserGroup, IconSetting } from 'views/components/icon';

function FeedHeader({ detail_feed, selected_feed, user_level, handlerAccountSpam, handlerEndConversation }) {
    const [data_assign, setDataAssign] = useState('');
    const [navigate, setNavigate] = useState('');

    return (
        <div>
            { data_assign.action === 'social_media_assign' && <TransferAssignedAgent values={data_assign} />}
            {
                navigate === 'Journey' &&
                selected_feed.customer_id &&
                <CustomerJourney customer={selected_feed} />
            }

            <div className="d-flex align-items-center justify-content-between">
                <div className="d-flex align-items-center">
                    <div className="symbol symbol-45 symbol-light mr-5">
                        <span className="symbol-label">
                            <IconLayoutPanel className="svg-icon svg-icon-lg svg-icon-primary" />
                        </span>
                    </div>
                    <div className="d-flex flex-column flex-grow-1">
                        <span className="text-dark-75 mb-1 font-size-lg font-weight-bolder">{detail_feed.page_name}</span>
                        <div className="d-flex">
                            <div className="d-flex align-items-center pr-5">
                                <IconClock className="svg-icon svg-icon-md svg-icon-primary pr-1" />
                                <span className="text-muted font-weight-bold">{(detail_feed.created_at).replace('T', ' ').slice(0, 19)}</span>
                            </div>
                            <div className="d-flex align-items-center">
                                <span className="svg-icon svg-icon-md svg-icon-primary pr-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                            <rect x={0} y={0} width={24} height={24} />
                                            <path d="M5.5,4 L9.5,4 C10.3284271,4 11,4.67157288 11,5.5 L11,6.5 C11,7.32842712 10.3284271,8 9.5,8 L5.5,8 C4.67157288,8 4,7.32842712 4,6.5 L4,5.5 C4,4.67157288 4.67157288,4 5.5,4 Z M14.5,16 L18.5,16 C19.3284271,16 20,16.6715729 20,17.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,17.5 C13,16.6715729 13.6715729,16 14.5,16 Z" fill="#000000" />
                                            <path d="M5.5,10 L9.5,10 C10.3284271,10 11,10.6715729 11,11.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,12.5 C20,13.3284271 19.3284271,14 18.5,14 L14.5,14 C13.6715729,14 13,13.3284271 13,12.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z" fill="#000000" opacity="0.3" />
                                        </g>
                                    </svg>
                                </span>
                                <a href={detail_feed.permalink_url} target="_blank" className="text-muted font-weight-bold text-hover-primary">permalink url</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-flex align-items-center">
                    {
                        user_level === 'L1' &&
                        <button type="button" className="btn btn-icon btn-sm btn-light-danger btn-circle mr-4" title="End Conversation" onClick={() => handlerEndConversation(selected_feed)}>
                            <i className="far fa-window-close fa-sm"></i>
                        </button>
                    }
                    <button className="btn btn-sm btn-light-primary btn-icon" data-toggle="dropdown" aria-expanded="false">
                        <IconSetting className="svg-icon svg-icon-md" />
                    </button>
                    <div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                        <ul className="navi flex-column navi-hover py-2">
                            <li className="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2"> Choose an action: </li>
                            <li className="navi-item">
                                <NavLink to={`/customer/${selected_feed.customer_id}/edit`} className="navi-link">
                                    <IconUserGroup className="navi-icon" /> Detail Customer
                                </NavLink>
                            </li>
                            <li className="navi-item">
                                <Link to="#" onClick={() => setDataAssign({ action: 'social_media_assign', current_agent: selected_feed.agent_handle, interaction_id: selected_feed.chat_id })} data-toggle="modal" data-target="#modalTransferAssignedAgent" className="navi-link">
                                    <IconMark className="navi-icon" /> Assigned Agent
                                </Link>
                            </li>
                            <li className="navi-item">
                                <Link to="#" onClick={() => setNavigate('Journey')} className="navi-link" data-toggle="modal" data-target="#modalJourneyCustomer">
                                    <i className="fas fa-route fa-lg mr-2"></i> Journey
                                </Link>
                            </li>
                            <li className="navi-item">
                                <div className="dropdown-divider"></div>
                            </li>
                            <li className="navi-item">
                                <Link to="#" onClick={() => handlerAccountSpam(selected_feed)} className="navi-link">
                                    <i className="far fa-eye-slash fa-lg mr-2"></i> Block Comment
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FeedHeader