import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import IconBrand from 'views/components/IconBrand';
import DurationTimer from 'views/components/DurationTimer';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { baseUrl } from 'app/config';
import { ButtonRefresh } from 'views/components/button';
import { apiListInboxFeeds } from 'app/services/apiSosmed';
import { authUser } from 'app/slice/sliceAuth';

function FeedInboxList({ list_inbox_feeds, selected_feed, handlerSelectFeed, status }) {
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);

    return (
        <Card>
            <CardHeader>
                <CardTitle title="Feed & Comments" subtitle={status.socket_id !== null ? <span className="text-success">Available</span> : 'Not Ready'} />
                <CardToolbar>
                    <span className="font-weight-bold font-size-sm mr-2">Live</span>
                    <span className="label label-rounded label-light-primary">{list_inbox_feeds ? list_inbox_feeds.length : 0}</span>
                    <ButtonRefresh onClick={() => dispatch(apiListInboxFeeds({ agent_handle: username }))} />
                </CardToolbar>
            </CardHeader>
            <CardBody className="p-0">
                <div style={{ height: 'calc(75vh - 60px)', overflow: 'auto' }}>
                    {
                        list_inbox_feeds?.map((customer, index) => {
                            return (
                                <div className="list list-hover border-bottom" key={index} onClick={() => handlerSelectFeed(customer)}>
                                    <div className={`list-item d-flex align-items-center justify-content-between ${customer.chat_id === selected_feed?.chat_id ? 'active' : ''}`}>
                                        <div className="d-flex align-items-center w-50 py-4 mx-2">
                                            <div className="symbol-group symbol-hover">
                                                <div className="symbol symbol-circle">
                                                    {
                                                        customer?.media_url.slice(customer?.media_url.indexOf('.') + 1) === 'jpg'
                                                            ? <img src={baseUrl + '/' + customer?.media_url} alt={customer.post_id} height={45} width={45} />
                                                            : <span className="symbol-label font-size-lg font-weight-bold text-uppercase bg-secondary">
                                                                <i className="fas fa-photo-video text-info" />
                                                            </span>
                                                    }
                                                </div>
                                                <div className="symbol symbol-circle">
                                                    <IconBrand name={(customer.channel).split('_')[0]} height={20} width={20} />
                                                </div>
                                            </div>

                                            <div className="flex-grow-1 mx-2">
                                                <div className="mr-2">{customer.customer_name}</div>
                                                <div className="text-muted mt-2">{customer.user_id}</div>
                                            </div>
                                        </div>
                                        <div className="d-flex flex-column align-items-end mx-2">
                                            <div className="d-flex align-items-center justify-content-between mt-2">
                                                <span className="label label-light-primary font-weight-bold label-inline mx-1">{customer.channel}</span>
                                                <span className={`label label-sm label-rounded mx-1 ${customer.total_chat > 0 ? 'label-danger' : 'label-success'}`}>
                                                    {customer.total_chat}
                                                </span>
                                            </div>
                                            <span className="text-muted mt-2 font-size-sm">{customer.page_name}</span>
                                            <DurationTimer duration={customer.handling_duration} />
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </CardBody>
        </Card>
    )
}

export default FeedInboxList