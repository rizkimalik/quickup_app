import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2';
import { NavLink, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import SplashScreen from 'views/components/SplashScreen';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { FeedContent, FeedConversation, FeedHeader, FeedInboxList } from '.';
import { Card, CardBody } from 'views/components/card';
import { socket } from 'app/config';
import { authUser } from 'app/slice/sliceAuth';
import { Datetime } from 'views/components/Datetime';
import { setTicketThread } from 'app/slice/sliceTicket';
import { setSelectedInboxFeed, setDetailFeed } from 'app/slice/sliceSosmed';
import { IconClock, IconGroupChat } from 'views/components/icon';
import { apiListInboxFeeds, apiShowDetailFeed, apiConversationFeed, setAccountSpam, getEndChat } from 'app/services/apiSosmed';

function SocialMediaFeed() {
    const dispatch = useDispatch();
    const history = useHistory();
    const [message, setMessage] = useState('');
    const [comment_id, setCommentID] = useState('');
    const [loading, setLoading] = useState(false);
    const [conversation_feeds, setConversation] = useState([]);
    const [return_inbox_feed, setReturnInboxFeed] = useState(false);
    const { username, user_level } = useSelector(authUser);
    const { list_inbox_feeds, selected_feed, detail_feed, conversation_feed, status } = useSelector(state => state.sosialmedia);

    useEffect(() => {
        dispatch(apiListInboxFeeds({ agent_handle: username }));
    }, [dispatch, username, return_inbox_feed]);

    useEffect(() => {
        function showMessageContent(res) {
            setReturnInboxFeed(true);
            if (selected_feed) {
                if (res.chat_id === selected_feed.chat_id) {
                    setConversation(conversation_feeds => [...conversation_feeds, res]);
                }
            }
        }

        socket.on('return-instagram-feed', (res) => {
            showMessageContent(res);
        });
        socket.on('return-facebook-feed', (res) => {
            showMessageContent(res);
        });
        setReturnInboxFeed(false);
    }, [selected_feed]);

    useEffect(() => {
        setConversation(conversation_feed.data);
    }, [conversation_feed]);

    function sendReplyMessage(e) {
        e.preventDefault();

        let message_type = 'text';
        let values = {
            username: selected_feed.agent_handle,
            chat_id: selected_feed.chat_id,
            user_id: selected_feed.user_id,
            customer_id: selected_feed.customer_id,
            message: message,
            message_type: message_type,
            name: username,
            email: selected_feed.email,
            channel: selected_feed.channel,
            flag_to: 'agent',
            agent_handle: username,
            page_id: selected_feed.page_id,
            post_id: selected_feed.post_id,
            comment_id: comment_id,
            date_create: Datetime(),
            attachment: '',
            typing: false
        }

        if (message) {
            if (values.channel === 'Instagram_Feed') {
                socket.emit('send-instagram-feed', values);
            }
            else if (values.channel === 'Facebook_Feed') {
                socket.emit('send-facebook-feed', values);
            }
            setConversation(conversation_feeds => [...conversation_feeds, values]);
            setTimeout(() => {
                dispatch(apiListInboxFeeds({ agent_handle: username }))
            }, 500);
        }
        setMessage('');
    }

    function handlerSelectFeed(detail) {
        socket.emit('join-room', detail.chat_id);
        setLoading(true);
        dispatch(setSelectedInboxFeed(detail));
        dispatch(apiListInboxFeeds({ agent_handle: username }));
        setTimeout(() => {
            dispatch(apiShowDetailFeed({ post_id: detail.post_id }));
        }, 500);
        setTimeout(() => {
            dispatch(apiConversationFeed({ chat_id: detail.chat_id }));
            setLoading(false);
        }, 1000);
    }

    function handlerAccountSpam(value) {
        Swal.fire({
            title: 'Spam Chat.',
            text: 'Do you want to report the chat?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Report',
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(setAccountSpam(value));
                dispatch(apiListInboxFeeds({ agent_handle: username }))
                dispatch(setSelectedInboxFeed({}))
                dispatch(apiConversationFeed({ chat_id: value.chat_id }))
            }
        })
    }

    function handlerEndConversation(value) {
        const { chat_id, customer_id, user_id, channel } = value;
        Swal.fire({
            title: 'End Session',
            text: 'Do you want to close the conversations?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'End Session',
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: 'Add to Ticket',
                    text: 'open form create ticket now.',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Create Ticket',
                    cancelButtonText: 'No',
                }).then((res) => {
                    if (res.isConfirmed) {
                        dispatch(getEndChat({ chat_id, customer_id }));
                        setConversation([]);
                        dispatch(setDetailFeed({}));
                        dispatch(setSelectedInboxFeed({}));
                        history.push(`/ticket?thread_id=${chat_id}&channel=${channel}&account=${user_id}`);
                    }
                    else {
                        dispatch(setTicketThread({
                            thread_id: chat_id,
                            thread_channel: channel,
                            account: user_id,
                            subject: 'interaction data sosmed.'
                        }));
                        dispatch(getEndChat({ chat_id, customer_id }));
                        setConversation([]);
                        dispatch(setDetailFeed({}));
                        dispatch(setSelectedInboxFeed({}));
                        dispatch(apiListInboxFeeds({ agent_handle: username }));
                    }
                })
            }
        })
    }

    return (
        <MainContent>
            <SubHeader active_page="Channels" menu_name="Social Media" modul_name="Feed & Comments">
                <NavLink to="/omnichannel/socmed" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                    <IconGroupChat className="svg-icon svg-icon-sm" /> Live Messages
                </NavLink>
                <NavLink to="/omnichannel/history" className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                    <IconClock className="svg-icon svg-icon-sm" /> History Social Media
                </NavLink>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-4">
                        <FeedInboxList
                            status={status}
                            list_inbox_feeds={list_inbox_feeds}
                            selected_feed={selected_feed}
                            handlerSelectFeed={handlerSelectFeed}
                        />
                    </div>
                    <div className="col-lg-8">
                        {loading && <SplashScreen />}
                        <Card>
                            <CardBody className="p-8">
                                {
                                    detail_feed.post_id && // show if data ready
                                    <div>
                                        <FeedHeader
                                            detail_feed={detail_feed}
                                            selected_feed={selected_feed}
                                            user_level={user_level}
                                            handlerAccountSpam={handlerAccountSpam}
                                            handlerEndConversation={handlerEndConversation}
                                        />
                                        <FeedContent
                                            conversation_feeds={conversation_feeds}
                                            detail_feed={detail_feed}
                                        />
                                    </div>
                                }
                                <FeedConversation
                                    conversation_feeds={conversation_feeds}
                                    sendReplyMessage={sendReplyMessage}
                                    setCommentID={setCommentID}
                                    comment_id={comment_id}
                                    setMessage={setMessage}
                                    message={message}
                                    user_level={user_level}
                                    selected_feed={selected_feed}
                                />
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default SocialMediaFeed