import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { authUser } from 'app/slice/sliceAuth';
import { ButtonRefresh } from 'views/components/button'
import { apiEmailHistory } from 'app/services/apiEmail';
import { IconMailOpen, IconMailReply } from 'views/components/icon';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { EmailDetail, EmailReply } from '.';

function EmailHistory() {
    const dispatch = useDispatch();
    const user = useSelector(authUser);
    const [email_detail, setEmailDetail] = useState('');
    const { email_history } = useSelector(state => state.email);

    useEffect(() => {
        dispatch(apiEmailHistory({ agent_handle: user.username, user_level: user.user_level }))
    }, [dispatch, user]);

    function componentButtonActions({ data }) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button type="button" onClick={() => setEmailDetail({ ...data, ...{ action: 'email_detail' } })} className="btn btn-icon btn-light-info btn-hover-info btn-xs mx-1" title="Detail Mail" data-toggle="modal" data-target="#modalEmailDetail">
                    <IconMailOpen className="svg-icon svg-icon-xs" />
                </button>
                <button type="button" onClick={() => setEmailDetail({ ...data, ...{ action: 'email_reply' } })} className="btn btn-icon btn-light-success btn-hover-info btn-xs mx-1" title="Reply Mail" data-toggle="modal" data-target="#modalEmailReply">
                    <IconMailReply className="svg-icon svg-icon-xs" />
                </button>
            </div>
        )
    }

    return (
        <div className="rounded p-4 my-2">
            {email_detail.action === 'email_detail' && <EmailDetail email_detail={email_detail} />}
            {email_detail.action === 'email_reply'  && <EmailReply email_detail={email_detail} />}

            <Card>
                <CardHeader className="border-bottom">
                    <CardTitle title="Data Email History" subtitle="Email data has been converted to ticket." />
                    <CardToolbar>
                        <ButtonRefresh onClick={() => dispatch(apiEmailHistory({ agent_handle: user.username, user_level: user.user_level }))} />
                    </CardToolbar>
                </CardHeader>
                <CardBody className="p-4">
                    <DataGrid
                        dataSource={email_history}
                        remoteOperations={{
                            filtering: true,
                            sorting: true,
                            paging: true
                        }}
                        allowColumnReordering={true}
                        allowColumnResizing={true}
                        columnAutoWidth={true}
                        showBorders={true}
                        showColumnLines={true}
                        showRowLines={true}
                        columnWidth={150}
                    >
                        <HeaderFilter visible={true} />
                        <FilterRow visible={true} />
                        <Paging defaultPageSize={10} />
                        <Pager
                            visible={true}
                            allowedPageSizes={[10, 20, 50]}
                            displayMode='full'
                            showPageSizeSelector={true}
                            showInfo={true}
                            showNavigationButtons={true} />
                        <Column caption="Actions" dataField="id" fixed={true} width={130} cellRender={componentButtonActions} />
                        <Column caption="Email ID" dataField="email_id" fixed={true} />
                        <Column caption="Ticket Number" dataField="ticket_number" />
                        <Column caption="Agent" dataField="agent_handle" />
                        <Column caption="Subject" dataField="esubject" />
                        <Column caption="From" dataField="efrom" />
                        <Column caption="To" dataField="eto" />
                        <Column caption="Cc" dataField="ecc" />
                        <Column caption="Date Email" dataField="date_email" dataType="datetime" />
                        <Column caption="Date Receive" dataField="date_receive" dataType="datetime" />
                        <Column caption="Date Assign" dataField="date_blending" dataType="datetime" />
                    </DataGrid>
                </CardBody>
            </Card>
        </div>
    )
}

export default EmailHistory