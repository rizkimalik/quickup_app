import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal';
import { EmailDataInteraction, EmailReply } from '.';
import { apiEmailDetail } from 'app/services/apiEmail';
import SplashScreen from 'views/components/SplashScreen';

function EmailDetail({ email_detail }) {
    const dispatch = useDispatch();
    const [open_view, setOpenView] = useState('email_detail');
    const [email_data_detail, setEmailDetail] = useState('');
    // const { email_data_detail } = useSelector(state => state.email);

    useEffect(async () => {
        setEmailDetail('');
        const { payload } = await dispatch(apiEmailDetail({ email_id: email_detail.email_id }));
        if (payload.status === 200) {
            setEmailDetail(payload.data);
        }
        else {
            setEmailDetail('');
        }
    }, [dispatch, email_detail]);

    return (
        <Modal id="modalEmailDetail" modal_size="modal-xl">
            <ModalHeader title={`ID: #${email_detail.email_id}`} />
            <ModalBody className="p-0">
                {!email_data_detail && <SplashScreen />}
                <EmailReply email_detail={email_detail} />

                <div className="row">
                    <div className="col-lg-12 m-0">
                        {
                            open_view === 'email_interaction'
                                ? <div className="p-5" style={{ height: '500px', overflow: 'auto' }}>
                                    <EmailDataInteraction data={email_data_detail} />
                                </div>
                                : <div>
                                    <div className="d-flex p-5 flex-row flex-md-row flex-lg-row flex-xxl-row justify-content-between">
                                        <div className="d-flex align-items-center">
                                            <span className="symbol symbol-50 mr-4 border">
                                                <span className="symbol-label">
                                                    <i className="fa fa-envelope text-primary"></i>
                                                </span>
                                            </span>
                                            <div className="d-flex flex-column flex-grow-1 flex-wrap mr-2">
                                                <div className="d-flex">
                                                    <span className="font-size-lg font-weight-bolder text-wrap text-dark-75 text-hover-primary mr-2">{(email_data_detail.efrom)?.replaceAll(';', '; ')}</span>
                                                </div>
                                                <div className="d-flex flex-column">
                                                    <div className="toggle-off-item">
                                                        <span className="font-weight-bold text-muted text-wrap">
                                                            {/* add space to wrap text */}
                                                            To: {(email_data_detail.eto)?.replaceAll(';', '; ')}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="d-flex flex-column">
                                                    <div className="toggle-off-item">
                                                        <span className="font-weight-bold text-muted text-wrap">
                                                            Cc: {(email_data_detail.ecc)?.replaceAll(';', '; ')}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="d-flex my-2 my-xxl-0 align-items-md-center align-items-lg-center align-items-xxl-center flex-row flex-md-row flex-lg-row flex-xxl-row">
                                            <div className="font-weight-bold text-muted mx-2">{(email_data_detail.date_email)?.replace('T', ' ').substring(19, 0)}</div>
                                            <div className="d-flex align-items-center flex-wrap flex-xxl-nowrap" data-inbox="toolbar">
                                                <button type="button" data-toggle="modal" data-target="#modalEmailReply" className="btn btn-clean btn-sm btn-icon" title="Reply">
                                                    <i className="fas fa-reply icon-1x" />
                                                </button>
                                                <span className="btn btn-clean btn-sm btn-icon" data-toggle="tooltip" data-placement="top" title="Settings">
                                                    <i className="flaticon-more icon-1x" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="font-weight-bold font-size-h3 mb-3 px-5">{email_data_detail.esubject}</div>
                                    <div className="p-5 border-top" style={{ height: '400px', overflow: 'auto' }}>
                                        <div dangerouslySetInnerHTML={{ __html: email_data_detail.ebody_html }}></div>
                                    </div>
                                </div>
                        }
                    </div>
                </div>
            </ModalBody>
            <ModalFooter>
                <button type="button" onClick={() => setOpenView('email_detail')} className="btn btn-light btn-sm font-weight-bolder mx-2"><i className="fas fa-envelope-open-text icon-1x" /> Detail</button>
                <button type="button" onClick={() => setOpenView('email_interaction')} className="btn btn-light btn-sm font-weight-bolder mx-2"><i className="fas fa-mail-bulk icon-1x" /> Interaction</button>
                <button type="button" className="btn btn-light-primary btn-sm font-weight-bolder mx-2" data-toggle="modal" data-target="#modalEmailReply"><i className="fas fa-reply icon-1x" /> Reply</button>
            </ModalFooter>
        </Modal>
    )
}

export default EmailDetail