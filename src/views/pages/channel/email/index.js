import EmailInbox from './EmailInbox'
import EmailSent from './EmailSent'
import EmailCompose from './EmailCompose'
import EmailDetail from './EmailDetail'
import EmailHistory from './EmailHistory'
import EmailReply from './EmailReply';
import EmailSpam from './EmailSpam';
import EmailDataInteraction from './EmailDataInteraction';

export {
    EmailInbox,
    EmailSent,
    EmailCompose,
    EmailDetail,
    EmailHistory,
    EmailReply,
    EmailSpam,
    EmailDataInteraction,
}