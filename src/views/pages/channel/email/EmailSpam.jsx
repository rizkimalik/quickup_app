import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { ButtonRefresh } from 'views/components/button'
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { apiDataEmailSpam, apiUpdateEmailUnSpam } from 'app/services/apiEmail';
import { IconMailOpen, IconMailHistory } from 'views/components/icon';
import { authUser } from 'app/slice/sliceAuth';
import { EmailDetail } from '.';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';

function EmailSpam() {
    const dispatch = useDispatch();
    const user = useSelector(authUser);
    const [email_detail, setEmailDetail] = useState('');
    const { email_data_spam } = useSelector(state => state.email);

    useEffect(() => {
        dispatch(apiDataEmailSpam({ agent_handle: user.username, user_level: user.user_level }));
    }, [dispatch]);

    async function handlerEmailUnSpam({ email_id }) {
        const { payload } = await dispatch(apiUpdateEmailUnSpam({ email_id }));
        if (payload.message === 'success') {
            dispatch(apiDataEmailSpam({ agent_handle: user.username, user_level: user.user_level }))
            SwalAlertSuccess('Success.', 'Data success move to Inbox.')
        }
        else {
            SwalAlertError('Failed.', 'Error unspam data email.')
        }
    }

    function componentButtonActions({ data }) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button type="button" onClick={() => setEmailDetail({ ...data, ...{ action: 'email_detail' } })} className="btn btn-icon btn-light-info btn-hover-info btn-xs mx-1" title="Detail Mail" data-toggle="modal" data-target="#modalEmailDetail">
                    <IconMailOpen className="svg-icon svg-icon-xs" />
                </button>
                <button type="button" onClick={() => handlerEmailUnSpam({ email_id: data.email_id })} className="btn btn-icon btn-light-primary btn-hover-primary btn-xs mx-1" title="Unspam Email">
                    <IconMailHistory className="svg-icon svg-icon-xs" />
                </button>
            </div>
        )
    }

    return (
        <div className="rounded rounded p-4 my-2">
            {email_detail.action === 'email_detail' && <EmailDetail email_detail={email_detail} />}
            <Card>
                <CardHeader className="border-bottom">
                    <CardTitle title="Data Email Spam" subtitle="Data all email spam." />
                    <CardToolbar>
                        <ButtonRefresh onClick={() => dispatch(apiDataEmailSpam({ agent_handle: user.username, user_level: user.user_level }))} />
                    </CardToolbar>
                </CardHeader>
                <CardBody className="p-4">
                    <DataGrid
                        dataSource={email_data_spam}
                        remoteOperations={{
                            filtering: true,
                            sorting: true,
                            paging: true
                        }}
                        allowColumnReordering={true}
                        allowColumnResizing={true}
                        columnAutoWidth={true}
                        showBorders={true}
                        showColumnLines={true}
                        showRowLines={true}
                        columnWidth={150}
                    >
                        <HeaderFilter visible={true} />
                        <FilterRow visible={true} />
                        <Paging defaultPageSize={10} />
                        <Pager
                            visible={true}
                            allowedPageSizes={[10, 20, 50]}
                            displayMode='full'
                            showPageSizeSelector={true}
                            showInfo={true}
                            showNavigationButtons={true} />
                        <Column caption="Actions" dataField="id" fixed={true} width={100} cellRender={componentButtonActions} />
                        <Column caption="Email ID" dataField="email_id" fixed={true} />
                        <Column caption="Subject" dataField="esubject" />
                        <Column caption="From" dataField="efrom" />
                        <Column caption="To" dataField="eto" />
                        <Column caption="Cc" dataField="ecc" />
                        <Column caption="Agent" dataField="agent_handle" />
                        <Column caption="Date Email" dataField="date_email" dataType="datetime" />
                        <Column caption="Date Receive" dataField="date_receive" dataType="datetime" />
                        <Column caption="Date Assign" dataField="date_blending" dataType="datetime" />
                    </DataGrid>
                </CardBody>
            </Card>
        </div>
    )
}

export default EmailSpam