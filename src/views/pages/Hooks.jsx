import React, { useCallback, useMemo, useRef, useState } from 'react'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'

function Hooks() {
    const refInput = useRef();
    const [count, setCount] = useState(0);
    const [todos, setTodos] = useState([]);
    const calculation = useMemo(() => expensiveCalculation(count), [count]);

    const increment = () => {
        setCount((c) => c + 1);
    }

    const addTodo = useCallback(() => {
        setTodos((t) => [...t, refInput.current.value]);
        console.log('render todo');
    }, []);

    return (
        <MainContent>
            <SubHeader active_page="Home" menu_name="Hook" modul_name="React" />
            <Container>
                <div>
                    <h2>Use Memo</h2>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <button onClick={addTodo} className="btn btn-primary" type="button">Add Todo</button>
                        </div>
                        <input type="text" ref={refInput} className="form-control" placeholder="Search for..." />
                    </div>
                    {todos.map((todo, index) => {
                        return <p key={index}>{todo}</p>;
                    })}
                </div>
                <hr />
                <div>
                    Count: {count}
                    <button onClick={increment} className="btn btn-primary">+</button>
                    <h2>Expensive Calculation: {calculation}</h2>
                    
                </div>
                <hr />
                <p>
                    The useCallback and useMemo Hooks are similar. The main difference is that useMemo returns a memoized value and useCallback returns a memoized function. You can learn more about useMemo in the useMemo chapter.
                </p>
            </Container>
        </MainContent>
    )
}

const expensiveCalculation = (num) => {
    console.log("Calculating...");
    for (let i = 0; i < 1000; i++) {
        num += 1;
    }
    return num;
};

export default Hooks