import React from 'react'
import { NavLink } from 'react-router-dom';
// import Icons from 'views/components/Icons';
const bg_card = ['bg-primary', 'bg-warning', 'bg-success', 'bg-danger', 'bg-info', 'bg-light', 'bg-dark'];

function BucketStatus({ total_ticket }) {
    // console.log(total_ticket.map((item) => item.total).reduce((prev, curr) => prev + curr, 0));
    return (
        <div className="row">
            {
                total_ticket.map((item, index) => {
                    return (
                        <div className="col-md-3" key={index}>
                            <NavLink to={`/todolist/${item.status}`} className={`card card-custom ${bg_card[index]} card-stretch gutter-b border`}>
                                <div className="card-body d-flex align-items-center justify-content-between p-8">
                                    <div className="d-flex">
                                        <i className={`icon-2x text-white ${item.icon}`} />
                                    </div>
                                    <div className="d-flex flex-column align-items-end">
                                        <span className="font-weight-bolder text-white font-size-h2 d-block">{item.total}</span>
                                        <span className="font-weight-bold text-white font-size-sm d-block">Ticket {item.status}</span>
                                    </div>
                                </div>
                            </NavLink>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default BucketStatus
