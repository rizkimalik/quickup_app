import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiStatusShow, apiStatusUpdate } from 'app/services/apiStatus'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import FormInput from 'views/components/FormInput';

function StatusEdit() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { id } = useParams();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        async function getStatusShow() {
            try {
                const { payload } = await dispatch(apiStatusShow({ id }))
                if (payload.status === 200) {
                    const {
                        id,
                        status,
                        active,
                        description
                    } = payload.data;
                    reset({
                        id,
                        status,
                        active,
                        description
                    });
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        getStatusShow();
    }, [id, dispatch, reset]);

    const onSubmitEditStatus = async (data) => {
        try {
            data.updated_by = username;
            const { payload } = await dispatch(apiStatusUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data.');
                history.push('/status');
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Status" modul_name="Status Edit" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Update Status" subtitle="Form update status." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitEditStatus)} className="form">
                        <CardBody>
                            <FormInput
                                name="status"
                                type="text"
                                label="Status Name"
                                className="form-control"
                                placeholder="Enter status name"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.status}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/status" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default StatusEdit