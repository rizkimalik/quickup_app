import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiDepartmentStore } from 'app/services/apiDepartment';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';

function DepartmentCreate() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { register, formState: { errors }, handleSubmit } = useForm();

    const onSubmitCreateDepartment = async (data) => {
        try {
            data.created_by = username;
            const { payload } = await dispatch(apiDepartmentStore(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Insert Success', 'Success into application.');
                history.push('/department')
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Department" modul_name="Department Create" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Add Department" subtitle="Form add new department." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitCreateDepartment)} className="form">
                        <CardBody>
                            <FormInput
                                name="department_id"
                                type="text"
                                label="Department ID"
                                className="form-control"
                                placeholder="Enter department id"
                                register={register}
                                rules={{ required: true, maxLength: 50 }}
                                readOnly={false}
                                errors={errors.department_id}
                            />
                            <FormInput
                                name="department_name"
                                type="text"
                                label="Department Name"
                                className="form-control"
                                placeholder="Enter department name"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.department_name}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/department" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default DepartmentCreate