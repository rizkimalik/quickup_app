import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiDepartmentShow, apiDepartmentUpdate } from 'app/services/apiDepartment'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import FormInput from 'views/components/FormInput';

function DepartmentEdit() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { department_id } = useParams();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        async function getDepartmentShow() {
            try {
                const { payload } = await dispatch(apiDepartmentShow({ department_id }))
                if (payload.status === 200) {
                    const {
                        id,
                        department_id,
                        department_name,
                        description,
                        active
                    } = payload.data;
                    reset({
                        id,
                        department_id,
                        department_name,
                        description,
                        active
                    });
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        getDepartmentShow();
    }, [department_id, dispatch, reset]);

    const onSubmitEditDepartment = async (data) => {
        try {
            data.updated_by = username;
            const { payload } = await dispatch(apiDepartmentUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data.');
                history.push('/department');
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Department" modul_name="Department Edit" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Update Department" subtitle="Form update department." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitEditDepartment)} className="form">
                        <CardBody>
                            <FormInput
                                name="department_id"
                                type="text"
                                label="Department ID"
                                className="form-control"
                                placeholder="Enter department id"
                                register={register}
                                rules={{ required: true, maxLength: 50 }}
                                readOnly={false}
                                errors={errors.department_id}
                            />
                            <FormInput
                                name="department_name"
                                type="text"
                                label="Department Name"
                                className="form-control"
                                placeholder="Enter department name"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.department_name}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/department" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default DepartmentEdit