import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import FormInput from 'views/components/FormInput';
import { apiChatTemplateShow,apiChatTemplateUpdate } from 'app/services/apiChatTemplate';

function ChatTemplateEdit() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { id } = useParams();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        async function getChatTemplateShow() {
            try {
                const { payload } = await dispatch(apiChatTemplateShow({ id }))
                if (payload.status === 200) {
                    const {
                        id,
                        short_code,
                        content_message,
                    } = payload.data;
                    reset({
                        id,
                        short_code,
                        content_message
                    });
                }
            }
            catch (error) {
                SwalAlertError('Failed.', error.message);
            }
        }
        getChatTemplateShow();
    }, [id, dispatch, reset]);

    const onSubmitEdit = async (data) => {
        try {
            data.updated_by = username;
            const { payload } = await dispatch(apiChatTemplateUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data.');
                history.push('/master/chat_template');
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', error.message)
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Chat Template" modul_name="Form Edit" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Chat Template" subtitle="Form Update Chat Template." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitEdit)} className="form">
                        <CardBody>
                            <FormInput
                                name="short_code"
                                type="text"
                                label="Title"
                                className="form-control"
                                placeholder="Enter Title"
                                register={register}
                                rules={{ required: true, maxLength: 50 }}
                                readOnly={false}
                                errors={errors.short_code}
                            />
                            <FormInput
                                name="content_message"
                                type="textarea"
                                label="Content Template"
                                className="form-control"
                                placeholder="Enter Content Template"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.content_message}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/master/chat_template" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default ChatTemplateEdit