import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import DataGrid, { Column, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';
import { ButtonCreate, ButtonDelete, ButtonEdit, ButtonRefresh } from 'views/components/button';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { apiChatTemplateList,apiChatTemplateDelete } from 'app/services/apiChatTemplate';

function ChatTemplateList() {
    const dispatch = useDispatch();
    const { chat_templates } = useSelector(state => state.chat_template)

    useEffect(() => {
        dispatch(apiChatTemplateList())
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Chat Templates" modul_name="">
                <ButtonCreate to="/master/chat_template/create" />
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardTitle title="Chat Templates" subtitle="There are saved reply templates which can be used to quickly send out a reply to a conversation." />
                        <CardToolbar>
                            <ButtonRefresh onClick={(e) => dispatch(apiChatTemplateList())} />
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        <DataGrid
                            dataSource={chat_templates}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true
                            }}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            columnWidth={150}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                allowedPageSizes={[10, 20, 50]}
                                displayMode='full'
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Column caption="Actions" width={100} cellRender={({ data }) => {
                                return (
                                    <div className="d-flex align-items-end justify-content-center">
                                        <ButtonEdit to={`/master/chat_template/${data.id}/edit`} />
                                        <ButtonDelete onClick={(e) => apiChatTemplateDelete(data.id)} />
                                    </div>
                                )
                            }} />
                            <Column caption="Title" dataField="short_code" />
                            <Column caption="Content Template" width={350} dataField="content_message" />
                            <Column caption="Created By" dataField="created_by" />
                            <Column caption="Created At" dataField="created_at" dataType="date" format="MM/dd/yyyy hh:mm" />
                            <Column caption="Updated By" dataField="updated_by" />
                            <Column caption="Updated At" dataField="updated_at" dataType="date" format="MM/dd/yyyy hh:mm" />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default ChatTemplateList