import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging, Selection } from 'devextreme-react/data-grid'
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal'
import { apiCustomerRegistered } from 'app/services/apiCustomer';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { apiInsertCustomerSLA, apiAssignSLA_ByCustomer } from 'app/services/apiCategory';
import { authUser } from 'app/slice/sliceAuth';

function AssignModal({ category_sublv3_id }) {
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { customer_registered } = useSelector(state => state.customer);
    const [selected_row, setSelectedRowData] = useState('');
    const [sla, setSLA] = useState(0);

    useEffect(() => {
        dispatch(apiCustomerRegistered());
    }, [dispatch]);

    const submitAssignSLACustomer = async () => {
        const { payload } = await dispatch(apiInsertCustomerSLA({
            category_sublv3_id: category_sublv3_id,
            customer_id: selected_row.customer_id,
            created_by: username,
            sla: sla,
        }))

        if (payload.status === 200) {
            dispatch(apiAssignSLA_ByCustomer({ category_sublv3_id }));
            SwalAlertSuccess('Succsessfuly', payload.data[0].message);
        }
        else {
            SwalAlertError('Failed.', payload.data[0].message);
        }
    }

    return (
        <Modal id="modalAssignSLACustomer" modal_size="modal-lg">
            <ModalHeader title="Assign SLA by Customer" />
            <ModalBody>
                <div className="list border rounded mb-4">
                    <div className='list-item d-flex align-items-center justify-content-between'>
                        <div className="d-flex align-items-center py-2 mx-2">
                            <span className="bullet bullet-bar bg-success align-self-stretch mr-2"></span>
                            <div className="symbol symbol-40px">
                                <div className="symbol-label">
                                    <i className="fas fa-user text-success"></i>
                                </div>
                            </div>
                            <div className="flex-grow-1 mx-5">
                                <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">{selected_row.name}</div>
                                <div className="mt-2">
                                    <span className="text-mute m-2">{selected_row.customer_id}</span>
                                </div>
                            </div>

                            <div className="flex-grow-1 mx-5">
                                <div className="text-dark-75 font-weight-bolder text-hover-dark mb-1 font-size-lg mx-2">SLA (Days)</div>
                                <div className="mt-2">
                                    <input type="number" value={sla} onChange={(e) => setSLA(e.target.value)} className="form-control form-control-sm" />
                                </div>
                            </div>
                        </div>
                        <div className="d-flex flex-column align-items-end mx-2">
                            <button type="button" onClick={() => submitAssignSLACustomer()} className="btn btn-light-primary font-weight-bold" data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                </div>

                <DataGrid
                    dataSource={customer_registered}
                    remoteOperations={true}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                    columnMinWidth={150}
                    hoverStateEnabled={true}
                    onSelectionChanged={({ selectedRowsData }) => {
                        setSelectedRowData(selectedRowsData[0])
                    }}
                >
                    <Selection mode="single" />
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={5} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[5, 10, 20]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="CustomerID" dataField="customer_id" cellRender={(data) => {
                        return <span type="button" className="btn btn-sm btn-light-primary py-1 px-2">
                            {data.value}
                        </span>
                    }} />
                    <Column caption="Name" dataField="name" />
                    <Column caption="Email" dataField="email" />
                    <Column caption="Phone Number" dataField="phone_number" />
                </DataGrid>
            </ModalBody>
            <ModalFooter />
        </Modal>
    )
}

export default AssignModal