import React, { useEffect, useState } from 'react'
import { DropDownBox } from 'devextreme-react/drop-down-box';
import { DataGrid, Column, Paging, Scrolling, Selection } from 'devextreme-react/data-grid';

function ListDataCustomer({ data }) {
    const [opened, setOpened] = useState(false);
    const [selected_row, setSelectedRow] = useState('');

    useEffect(() => {
        setSelectedRow(data.value)
    }, [data])

    return (
        <DropDownBox
            onOptionChanged={(e) => {
                if (e.name === 'opened') {
                    setOpened(e.value)
                }
            }}
            opened={opened}
            dropDownOptions={{ width: 500 }}
            dataSource={data.column.lookup.dataSource}
            value={selected_row}
            displayExpr="name"
            valueExpr="customer_id"
            inputAttr={{ 'aria-label': 'customer_id' }}
            contentRender={() => {
                return (
                    <DataGrid
                        dataSource={data.column.lookup.dataSource}
                        remoteOperations={true}
                        height={250}
                        selectedRowKeys={data.value}
                        hoverStateEnabled={true}
                        onSelectionChanged={(selection_change) => {
                            console.log(selection_change);
                            // setSelectedRow(selection_change.selectedRowsData);
                            setOpened(false);
                            setSelectedRow(selection_change.selectedRowsData[0].customer_id);
                        }}
                        focusedRowEnabled={true}
                        defaultFocusedRowKey={data.value}
                    >
                        <Column caption="Customer ID" dataField="customer_id" />
                        <Column caption="Customer Name" dataField="name" />
                        <Column caption="Email" dataField="email" />
                        <Column caption="Phone Number" dataField="phone_number" />
                        <Paging enabled={true} defaultPageSize={10} />
                        <Scrolling mode="virtual" />
                        <Selection mode="single" />
                    </DataGrid>
                );
            }}>
        </DropDownBox>
    );
}

export default ListDataCustomer