import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiOrganizationShow, apiOrganizationUpdate } from 'app/services/apiOrganization'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import FormInput from 'views/components/FormInput';

function OrganizationEdit() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { id } = useParams();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        async function getOrganizationShow() {
            try {
                const { payload } = await dispatch(apiOrganizationShow({ id }))
                if (payload.status === 200) {
                    const {
                        id,
                        organization_name,
                        description
                    } = payload.data;
                    reset({
                        id,
                        organization_name,
                        description
                    });
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        getOrganizationShow();
    }, [id, dispatch, reset]);

    const onSubmitEditOrganization = async (data) => {
        try {
            data.updated_by = username;
            const { payload } = await dispatch(apiOrganizationUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data.');
                history.push('/organization');
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Organization" modul_name="Organization Edit" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Update Organization" subtitle="Form update organization." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitEditOrganization)} className="form">
                        <CardBody>
                            <FormInput
                                name="organization_name"
                                type="text"
                                label="Organization Name"
                                className="form-control"
                                placeholder="Enter organization name"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.organization_name}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/organization" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default OrganizationEdit