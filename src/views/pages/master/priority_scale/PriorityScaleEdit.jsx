import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiPriorityScaleShow, apiPriorityScaleUpdate } from 'app/services/apiPriorityScale'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import FormInput from 'views/components/FormInput';

function PriorityScaleEdit() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { id } = useParams();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        async function getPriorityScaleShow() {
            try {
                const { payload } = await dispatch(apiPriorityScaleShow({ id }))
                if (payload.status === 200) {
                    const {
                        id,
                        priority_scale,
                        active,
                        description
                    } = payload.data;
                    reset({
                        id,
                        priority_scale,
                        description,
                        active
                    });
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        getPriorityScaleShow();
    }, [id, dispatch, reset]);

    const onSubmitEditPriorityScale = async (data) => {
        try {
            data.updated_by = username;
            const { payload } = await dispatch(apiPriorityScaleUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data.');
                history.push('/priority_scale');
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Priority Scale" modul_name="Priority Scale Edit" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Update Priority Scale" subtitle="Form update priority_scale." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitEditPriorityScale)} className="form">
                        <CardBody>
                            <FormInput
                                name="priority_scale"
                                type="text"
                                label="Priority Scale"
                                className="form-control"
                                placeholder="Enter name"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.priority_scale}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/priority_scale" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default PriorityScaleEdit