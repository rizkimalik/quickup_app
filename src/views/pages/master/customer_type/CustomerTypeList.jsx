import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';

import DataGrid, { Column, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';
import { ButtonCreate, ButtonDelete, ButtonEdit, ButtonRefresh } from 'views/components/button';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { apiCustomerTypeDelete, apiCustomerTypeList } from 'app/services/apiCustomerType';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';

function CustomerTypeList() {
    const dispatch = useDispatch();
    const { customer_type } = useSelector(state => state.customer_type)

    useEffect(() => {
        dispatch(apiCustomerTypeList())
    }, [dispatch]);

    async function deleteCustomerTypeHandler(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiCustomerTypeDelete({ id }));
                if (payload.status === 200) {
                    SwalAlertSuccess('Success Delete', 'Deleted from database.')
                    dispatch(apiCustomerTypeList())
                }
                else {
                    SwalAlertError('Failed Delete', 'Please try again.')
                }
            }
        });
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Customer Type" modul_name="">
                <ButtonCreate to="/customer_type/create" />
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardTitle title="Customer Type" subtitle="data customer type." />
                        <CardToolbar>
                            <ButtonRefresh onClick={(e) => dispatch(apiCustomerTypeList())} />
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        <DataGrid
                            dataSource={customer_type}
                            keyExpr="id"
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                allowedPageSizes={[10, 20, 50]}
                                displayMode='full'
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Column caption="Actions" width={100} cellRender={({ data }) => {
                                return (
                                    <div className="d-flex align-items-end justify-content-center">
                                        <ButtonEdit to={`customer_type/${data.id}/edit`} />
                                        <ButtonDelete onClick={(e) => deleteCustomerTypeHandler(data.id)} />
                                    </div>
                                )
                            }} />
                            <Column caption="Customer Type" dataField="type_name" />
                            <Column caption="Description" dataField="description" />
                            <Column caption="Active" dataField="active" width={100} cellRender={({ data }) => {
                                return (
                                    <div className="d-flex justify-content-center">
                                        <span className="switch switch-outline switch-icon switch-sm switch-primary">
                                            <label>
                                                <input type="checkbox" defaultChecked={data.active} />
                                                <span />
                                            </label>
                                        </span>
                                    </div>
                                )
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default CustomerTypeList