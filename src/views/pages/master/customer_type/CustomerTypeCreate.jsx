import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiCustomerTypeStore } from 'app/services/apiCustomerType';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';

function CustomerTypeCreate() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { register, formState: { errors }, handleSubmit } = useForm();

    const onSubmitCreateCustomerType = async (data) => {
        try {
            data.created_by = username;
            const { payload } = await dispatch(apiCustomerTypeStore(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Insert Success', 'Success into application.');
                history.push('/customer_type')
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Customer Type" modul_name="Customer Type Create" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Add Customer Type" subtitle="Form add new customer type." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitCreateCustomerType)} className="form">
                        <CardBody>
                            <FormInput
                                name="type_name"
                                type="text"
                                label="Customer Type Name"
                                className="form-control"
                                placeholder="Enter type name"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.type_name}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/customer_type" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default CustomerTypeCreate