import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { apiReferenceTicket } from 'app/services/apiTicket';
import { ButtonRefresh } from 'views/components/button';

function TicketReference({ isReferenceOpen, ticket }) {
    const dispatch = useDispatch();
    const { reference_ticket } = useSelector(state => state.ticket);

    useEffect(() => {
        if (isReferenceOpen === true) {
            dispatch(apiReferenceTicket({ group_ticket_number: ticket.group_ticket_number }))
        }
    }, [dispatch, isReferenceOpen, ticket])

    return (
        <div className="border rounded p-4 my-2">
            <div className="d-flex justify-content-between mb-5">
                <h4>Data Reference Ticket</h4>
                <ButtonRefresh onClick={(e) => dispatch(apiReferenceTicket({ group_ticket_number: ticket.group_ticket_number }))} />
            </div>
            <DataGrid
                dataSource={reference_ticket}
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    allowedPageSizes={[10, 20, 50]}
                    displayMode='full'
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Ticket Number" dataField="ticket_number" />
                <Column caption="Reference No" dataField="group_ticket_number" />
                <Column caption="CustomerID" dataField="customer_id" />
                <Column caption="Name" dataField="name" />
                <Column caption="Channel" dataField="ticket_source" />
                <Column caption="Channel Type" dataField="source_information" />
                <Column caption="Date Create" dataField="date_create" />
                <Column caption="User Create" dataField="user_create" />
                <Column caption="Status" dataField="status" />
            </DataGrid>
        </div>
    )
}

export default TicketReference