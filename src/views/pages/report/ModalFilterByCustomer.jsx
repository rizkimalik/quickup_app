import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { IconMark } from 'views/components/icon';
import { apiCustomerList } from 'app/services/apiCustomer';
import { ButtonRefresh } from 'views/components/button';
import { Modal,ModalHeader,ModalBody,ModalFooter } from 'views/components/modal';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

function ModalFilterByCustomer({onSelectedCustomer}) {
    const dispatch = useDispatch();
    const { customers } = useSelector(state => state.customer);

    useEffect(() => {
        dispatch(apiCustomerList())
    }, [dispatch]);

    function componentButtonActions(data) {
        const customer = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    data-dismiss="modal"
                    onClick={() => onSelectedCustomer(customer)}
                    title="Select Customer"
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> {data.value}
                </button>
            </div>
        )
    }

    return (
        <Modal id="ModalFilterByCustomer">
            <ModalHeader title="Customer List" />
            <ModalBody>
                <DataGrid
                    dataSource={customers}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                    columnMinWidth={150}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={5} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[5, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="CustomerID" dataField="customer_id" cellRender={componentButtonActions} />
                    <Column caption="Name" dataField="name" />
                    <Column caption="Email" dataField="email" />
                    <Column caption="Phone Number" dataField="phone_number" />
                    <Column caption="Address" dataField="address" />
                    <Column caption="Status" dataField="status" cellRender={(data) => {
                        return <span className={`text-${data.value === 'Registered' ? 'success' : 'warning'} label-inline`}>{data.value}</span>
                    }} />
                </DataGrid>
            </ModalBody>
            <ModalFooter>
                <ButtonRefresh onClick={() => dispatch(apiCustomerList())} />
            </ModalFooter>
        </Modal>
    )
}

export default ModalFilterByCustomer