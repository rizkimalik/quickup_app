import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import * as echarts from 'echarts';
import { Link } from 'react-router-dom';
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { apiReportSpeedAnswerMessages } from 'app/services/apiReport';
import { Container, MainContent, SubHeader } from 'views/layouts/partials'

function ReportSpeedAnswerMessages() {
    const dispatch = useDispatch();
    const refDataChart = useRef();
    const { report_speed_answer_messages } = useSelector(state => state.report);

    useEffect(() => {
        dispatch(apiReportSpeedAnswerMessages({ action: 'Today' }));
    }, [dispatch]);

    useEffect(() => {
        function LoadDataChart() {
            const colors = ['#EF6765', '#73c0df', '#3ba272', '#fb8451', '#01B5D8', '#48CAE2', '#91E0EF'];
            const initEchart = echarts.init(refDataChart.current);
            const option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                    data: ['Total', 'Percentage']
                },
                xAxis: {
                    type: 'category',
                    data: [],
                },
                yAxis: {
                    type: 'value',
                    name: 'Total',
                    axisLabel: {
                        formatter: '{value}'
                    }
                },
                series: [{
                    name: 'Total',
                    type: 'bar',
                    showBackground: true,
                    backgroundStyle: {
                        color: 'rgba(180, 180, 180, 0.2)'
                    },
                    data: []
                },
                {
                    name: 'Percentage',
                    type: 'line',
                    showBackground: true,
                    backgroundStyle: {
                        color: 'rgba(180, 180, 180, 0.2)'
                    },
                    data: []
                }]
            }

            let data_axies = [];
            let data_series_bar = [];
            let data_series_line = [];
            for (let i = 0; i < report_speed_answer_messages?.length; i++) {
                data_axies.push(report_speed_answer_messages[i].time_range);
                data_series_bar.push({
                    value: report_speed_answer_messages[i].total_count,
                    itemStyle: {
                        color: colors[i]
                    }
                });
                data_series_line.push({
                    value: report_speed_answer_messages[i].percentage,
                });
            }
            option.xAxis.data = data_axies;
            option.series[0].data = data_series_bar;
            option.series[1].data = data_series_line;

            option && initEchart.setOption(option);
        }
        LoadDataChart()
    })

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Speed Answer Messages" />
            <Container>
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between">
                        <h4 className="font-weight-bold">Report Speed Answer Messages</h4>
                        <ul className="nav nav-pills nav-pills-sm nav-dark-75">
                            <li className="nav-item">
                                <Link to="#tab_7days" className="nav-link py-2 px-4" data-toggle="tab" onClick={() => dispatch(apiReportSpeedAnswerMessages({ action: 'Last7Days' }))}>Last 7 Days</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="#tab_today" className="nav-link py-2 px-4 active" data-toggle="tab" onClick={() => dispatch(apiReportSpeedAnswerMessages({ action: 'Today' }))}>Today</Link>
                            </li>
                        </ul>
                    </div>

                    <div className="row my-4">
                        <div className="col-lg-6">
                            <DataGrid
                                dataSource={report_speed_answer_messages}
                                remoteOperations={{
                                    filtering: true,
                                    sorting: true,
                                    paging: true
                                }}
                                allowColumnReordering={true}
                                allowColumnResizing={true}
                                columnAutoWidth={true}
                                showBorders={true}
                                showColumnLines={true}
                                showRowLines={true}
                            >
                                <HeaderFilter visible={true} />
                                <FilterRow visible={true} />
                                <Paging defaultPageSize={20} />
                                <Pager
                                    visible={true}
                                    allowedPageSizes={[20, 50, 100]}
                                    displayMode='full'
                                    showPageSizeSelector={true}
                                    showInfo={true}
                                    showNavigationButtons={true} />
                                <GroupPanel visible={true} />
                                <Column caption="Speed Answer" dataField="time_range" cellRender={(data) => {
                                    return <span className="text-primary font-weight-bolder">{data.value}</span>
                                }} />
                                <Column caption="Total Call" dataField="total_count" />
                                <Column caption="Percentage" dataField="percentage" cellRender={(data) => {
                                    return <div className="d-flex mt-4 mt-sm-0">
                                        <div className="progress progress-xs mt-2 mb-2 flex-shrink-0 w-150px w-xl-150px">
                                            <div className="progress-bar bg-primary" role="progressbar" style={{ width: `${data.value ? data.value : 0}%` }} aria-valuenow={50} aria-valuemin={0} aria-valuemax={100} />
                                        </div>
                                        <span className="font-weight-bolder text-dark ml-4">{data.value ? data.value : 0} %</span>
                                    </div>
                                }} />
                            </DataGrid>
                        </div>
                        <div className="col-lg-6">
                            <div className="card border-0">
                                <div className="card-body p-0 bg-white" style={{ height: '400px', width: '100%' }} ref={refDataChart} />
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default ReportSpeedAnswerMessages