import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import Icons from 'views/components/Icons'
import FormInput from 'views/components/FormInput';
import { SwalAlertError } from 'views/components/SwalAlert';
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { apiReportSummaryTicket,apiReportSummaryTicketExport } from 'app/services/apiReport';

function ReportSummaryTicket() {
    const dispatch = useDispatch();
    const { report_summary_ticket } = useSelector(state => state.report);
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();

    useEffect(() => {
        let today = new Date();
        const date_start = new Date(new Date().setDate(new Date().getDate() - 30)).toISOString().slice(0, 10);
        const date_end = today.toISOString().slice(0, 10);
        setValue('date_start', date_start);
        setValue('date_end', date_end);
    }, [setValue]);

    const onSubmitReportSummaryTicket = async (data) => {
        try {
            const result = await dispatch(apiReportSummaryTicket(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value,
            }
            const result = await dispatch(apiReportSummaryTicketExport(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');
                worksheet.columns = [
                    { header: 'User', key: 'username' },
                    { header: 'Total Closed', key: 'total_handled' },
                    { header: 'User Level', key: 'user_level' },
                    { header: 'Team / Group', key: 'department_name' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:W1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.xlsx.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `Report_SummaryTicketClosed_${data.date_start}_${data.date_end}.xlsx`);
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Summary Ticket" />
            <Container>
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between mb-4">
                        <h4 className="font-weight-bold">Report Summary Ticket Closed</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitReportSummaryTicket)} id="formReportSummaryTicket">
                        <div className="row">
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_start"
                                    type="date"
                                    label="Start Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_start}
                                />
                            </div>
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_end"
                                    type="date"
                                    label="End Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_end}
                                />
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={report_summary_ticket}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={false} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="User" dataField="username" />
                    <Column caption="Total Closed" dataField="total_handled" />
                    <Column caption="User Level" dataField="user_level" />
                    <Column caption="Team / Group" dataField="department_name" />
                </DataGrid>
            </Container>
        </MainContent>
    )
}

export default ReportSummaryTicket