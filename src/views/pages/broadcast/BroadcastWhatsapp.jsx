import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Link } from 'react-router-dom';

import FormInput from 'views/components/FormInput';
import FormSelect from 'views/components/FormSelect';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { apiWhatsappChannelList, apiBroadcastSend } from 'app/services/apiBroadcast';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';

function BroadcastWhatsapp() {
    const dispatch = useDispatch();
    const [file_excel, setFileExcel] = useState('');
    const [action, setAction] = useState('');
    const [broadcast_source, setBroadcastSource] = useState('manual');
    const [loading, setLoading] = useState('');
    const { register, formState: { errors }, handleSubmit } = useForm();
    const { whatsapp_channel_list } = useSelector(state => state.broadcast);

    useEffect(() => {
        dispatch(apiWhatsappChannelList());
    }, [dispatch]);

    const onSubmitBroadcast = async (data) => {
        console.log(data);
        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiBroadcastSend(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Broadcast Action', 'Success send whatsapp broadcast');
                setLoading('');
            }
            else {
                SwalAlertError('Failed Action.', 'Please try again.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Failed Action.', `Please try again, ${error.message}.`);
            setLoading('');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();
        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet('Contact');
        worksheet.columns = [
            { key: 'phone_number' },
            { key: 'contact_name' },
        ]
        worksheet.addRows([
            { phone_number: '620858000400', contact_name: 'Adi' },
            { phone_number: '620858111500', contact_name: 'Ani' }
        ]);
        workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `contact_list.xlsx`);
        });
    }

    return (
        <MainContent>
            <SubHeader active_page="Broadcast" menu_name="Whatsapp" modul_name="Feature">
                {/* <ButtonCreate to="/category/create" /> */}
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardTitle title="Broadcast Feature" subtitle="Whatsapp Broadcast Setup" />
                        <CardToolbar>
                            <Link to="/broadcast/dashboard" className="btn btn-sm font-weight-bolder btn-light-info">
                                <i className="far fa-compass fa-sm" />
                                Dashboard
                            </Link>
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        <form onSubmit={handleSubmit(onSubmitBroadcast)} className="row" encType="multipart/form-data">
                            <div className="col-lg-4">
                                <h3>Broadcast Setup</h3>
                                <FormSelect
                                    name="page_id"
                                    label="Whatsapp Channel"
                                    className="form-control"
                                    register={register}
                                    rules={{ required: true }}
                                    errors={errors.page_id}
                                    onChange={() => ''}
                                >
                                    <option value="">-- select whatsapp number --</option>
                                    {
                                        whatsapp_channel_list.map((item, index) => {
                                            return <option value={item.page_id} key={index}>{item.page_id} - {item.page_name}</option>
                                        })
                                    }
                                </FormSelect>
                                <FormInput
                                    name="broadcast_title"
                                    type="text"
                                    label="Broadcast Name"
                                    className="form-control"
                                    placeholder="Enter Broadcast Name"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.broadcast_title}
                                />
                                <FormSelect
                                    name="message_type"
                                    label="Message Type"
                                    className="form-control"
                                    register={register}
                                    rules={{ required: true }}
                                    errors={errors.message_type}
                                    onChange={() => ''}
                                >
                                    <option value="text">Text</option>
                                </FormSelect>
                                <FormInput
                                    name="message"
                                    type="textarea"
                                    label="Message"
                                    className="form-control"
                                    placeholder="Message Content"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.message}
                                />
                            </div>
                            <div className="col-lg-4">
                                <h3>Recipients</h3>
                                <div className="form-group">
                                    <label>Select broadcast type you want to send</label>
                                    <div className="radio-list">
                                        <label className="radio" htmlFor="radio1">
                                            <input type="radio" defaultChecked={true} name="source" value="manual" id="radio1" {...register("source", { required: true })} onChange={(e) => setBroadcastSource(e.target.value)} />
                                            <span />
                                            Submit manual Phone Number
                                        </label>
                                        <label className="radio" htmlFor="radio2">
                                            <input type="radio" name="source" value="contact" id="radio2" {...register("source", { required: true })} onChange={(e) => setBroadcastSource(e.target.value)} />
                                            <span />
                                            Select contact Groups
                                        </label>
                                        <label className="radio" htmlFor="radio3">
                                            <input type="radio" name="source" value="excel" id="radio3" {...register("source", { required: true })} onChange={(e) => setBroadcastSource(e.target.value)} />
                                            <span />
                                            Upload from Excel
                                        </label>
                                    </div>
                                    {errors.source && <span className="form-text text-danger">*Please select broadcast type</span>}
                                </div>
                                {
                                    broadcast_source === 'manual' &&
                                    <FormInput
                                        name="phone_number"
                                        type="text"
                                        label="Whatsapp Number"
                                        className="form-control"
                                        placeholder="Type Whatsapp Number"
                                        register={register}
                                        rules={{ required: true }}
                                        readOnly={false}
                                        errors={errors.phone_number}
                                    />
                                }
                                {
                                    broadcast_source === 'contact' &&
                                    <FormSelect
                                        name="group_contact"
                                        label="Contact Groups"
                                        className="form-control"
                                        register={register}
                                        rules={{ required: true }}
                                        errors={errors.group_contact}
                                        onChange={() => ''}
                                    >
                                        <option value="">-- select contact group --</option>
                                        {/* <option value="123">123</option> */}
                                    </FormSelect>
                                }
                                {
                                    broadcast_source === 'excel' &&
                                    <div className="form-group">
                                        <label>File Browser</label>
                                        <div />
                                        <div className="custom-file">
                                            <input type="file" {...register("excel_file", { required: true })} className="custom-file-input" onChange={(e) => setFileExcel(e.target.files[0])} />
                                            <label className="custom-file-label" htmlFor="fileupload">{file_excel ? file_excel.name : 'Upload Excel'}</label>
                                        </div>
                                        {errors.excel_file && <span className="form-text text-danger">Please upload excel file.</span>}

                                        <div className="card card-custom gutter-b bg-light-success mt-5">
                                            <div className="card-body p-4">
                                                <div className="d-flex align-items-center justify-content-between flex-lg-wrap flex-xl-nowrap">
                                                    <div className="d-flex flex-column mr-2">
                                                        <p className="text-dark-50">
                                                            Please use the downloaded excel file.
                                                        </p>
                                                    </div>
                                                    <div className="ml-6 ml-lg-0 ml-xxl-6 flex-shrink-0">
                                                        <button type="button" onClick={handlerExportExcel} className="btn btn-sm font-weight-bolder btn-success">
                                                            <i className="fas fa-download fa-sm" />
                                                            Excel Template
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                }

                            </div>
                            <div className="col-lg-4">
                                <h3>Action</h3>
                                <div className="form-group">
                                    <label>Broadcast Action :</label>
                                    <select name="action" {...register("action", { required: true })} onChange={(e) => setAction(e.target.value)} className="form-control">
                                        <option value="send">Send Broadcast</option>
                                        <option value="queue">Add to Queue</option>
                                    </select>
                                </div>
                                {
                                    action === 'queue' &&
                                    <div className="form-group">
                                        <label>Schedule</label>
                                        <input type="datetime-local" className="form-control" {...register("schedule_at", { required: true })} />
                                        {errors.schedule_at && <span className="form-text text-danger">Please pick datetime.</span>}
                                    </div>
                                }

                                <div className="d-flex justify-content-between">
                                    <button type="submit" className={`btn btn-primary font-weight-bolder btn-sm m-1 ${loading}`}>
                                        Send Broadcast
                                    </button>
                                </div>
                            </div>
                        </form>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default BroadcastWhatsapp