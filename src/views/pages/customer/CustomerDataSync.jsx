import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { apiCustomer_DataSync, apiCustomerUnsyncronize } from 'app/services/apiCustomer';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertSuccess,SwalAlertError } from 'views/components/SwalAlert';

function CustomerDataSync({ customer_id }) {
    const dispatch = useDispatch();
    const { user_level } = useSelector(authUser);
    const { customer_syncronize } = useSelector(state => state.customer);

    useEffect(() => {
        if (customer_id) {
            dispatch(apiCustomer_DataSync({ syncronized_to: customer_id }))
        }
        else {
            dispatch(apiCustomer_DataSync())
        }
    }, [dispatch, customer_id]);

    const submitUnsyncronizeCustomer = async (data) => {
        const { payload } = await dispatch(apiCustomerUnsyncronize({
            customer_primary: data.syncronized_to,
            customer_sync: data.customer_id
        }))

        if (payload.status === 200) {
            SwalAlertSuccess('Succsessfuly', payload.data[0].message);
            if (customer_id) {
                dispatch(apiCustomer_DataSync({ syncronized_to: customer_id }))
            }
            else {
                dispatch(apiCustomer_DataSync())
            }
        }
        else {
            SwalAlertError('Failed.', payload.data[0].message);
        }
    }

    function componentButtonActions(data) {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button type="button" onClick={() => submitUnsyncronizeCustomer(row)} className="btn btn-icon btn-light-info btn-hover-info btn-sm mx-1" title="Unsync Customer">
                    <i className="fas fa-user-slash fa-sm"></i>
                </button>
            </div>
        )
    }

    return (
        <div>
            <DataGrid
                dataSource={customer_syncronize}
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
                columnWidth={150}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    allowedPageSizes={[10, 20, 50]}
                    displayMode='full'
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Actions" dataField="id" width={150} cellRender={componentButtonActions} />
                <Column caption="Status" dataField="status" cellRender={(data) => {
                    return <span className={`label label-md label-light-${data.value === 'Registered' ? 'success' : 'warning'} label-inline`}>{data.value}</span>
                }} />
                <Column caption="CustomerID" dataField="customer_id" />
                <Column caption="Sync To" dataField="syncronized_to" />
                <Column caption="Name" dataField="name" />
                <Column caption="Email" dataField="email" />
                <Column caption="Phone Number" dataField="phone_number" />
                <Column caption="Card ID" dataField="no_ktp" cellRender={(data) => {
                    return user_level !== 'Admin'
                        ? <span>{"*".repeat(data.value?.length)}</span>
                        : <span>{data.value}</span>
                }} />
                <Column caption="Source" dataField="source" />
                <Column caption="Gender" dataField="gender" />
                <Column caption="Address" dataField="address" />
            </DataGrid>
        </div>
    )
}

export default CustomerDataSync