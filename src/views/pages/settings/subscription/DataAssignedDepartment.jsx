import React, { useEffect } from 'react'
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { ButtonRefresh } from 'views/components/button'
import { apiAssignedDepartment } from 'app/services/apiSubscription';
import { Card, CardHeader, CardBody, CardTitle, CardToolbar } from 'views/components/card'
import { apiAssignedDepartmentDelete } from 'app/services/apiSubscription';

function DataAssignedDepartment({ detail_channel }) {
    const dispatch = useDispatch();
    const { assigned_department } = useSelector(state => state.subscription);

    useEffect(() => {
        dispatch(apiAssignedDepartment({ page_id: detail_channel.page_id }))
    }, [dispatch, detail_channel]);

    function onHandlerDelete(values) {
        Swal.fire({
            title: "Are you sure?",
            text: `${values.department_name} want to unlisted!`,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes"
        })
        .then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiAssignedDepartmentDelete(values));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Success.",
                        text: `Deleted from database!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiAssignedDepartment({ page_id: detail_channel.page_id }))
                }
            }
        });
    }

    function componentButtonActions(data) {
        const values = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button type="button" onClick={() => onHandlerDelete(values)} className="btn btn-icon btn-light-danger btn-xs mx-1" title="Remove">
                    <i className="fas fa-ban icon-sm"></i>
                </button>
            </div>
        )
    }

    return (
        <Card>
            <CardHeader>
                <CardTitle title="Assigned Department" subtitle="Default all department, assigned to specific department." />
                <CardToolbar>
                    <ButtonRefresh onClick={() => dispatch(apiAssignedDepartment({ page_id: detail_channel.page_id }))} />
                </CardToolbar>
            </CardHeader>
            <CardBody className="p-4">
                <DataGrid
                    dataSource={assigned_department}
                    remoteOperations={true}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Actions" dataField="department_id" width={100} cellRender={componentButtonActions} />
                    <Column caption="Department" dataField="department_name" />
                </DataGrid>
            </CardBody>
        </Card>
    )
}

export default DataAssignedDepartment