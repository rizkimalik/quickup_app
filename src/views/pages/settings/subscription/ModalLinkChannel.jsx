import React, { useEffect, useState } from 'react'
import QRCode from "react-qr-code";

import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal'
import { useDispatch } from 'react-redux';
import { apiWhatsappQRCode } from 'app/services/apiSubscription';

function ModalLinkChannel({ data_connect }) {
    const dispatch = useDispatch();
    const [qrcode, setQrcode] = useState('');

    async function CallApiQRCode() {
        setQrcode('');
        const { payload } = await dispatch(apiWhatsappQRCode({ host: data_connect.url_api, token: data_connect.token }));
        if (payload.message === 'success') {
            setQrcode(payload.data.data);
        }
        else {
            setQrcode('');
        }
    }

    useEffect(async () => {
        setQrcode('');
        const { payload } = await dispatch(apiWhatsappQRCode({ host: data_connect.url_api, token: data_connect.token }));
        if (payload.message === 'success') {
            setQrcode(payload.data.data);
        }
        else {
            setQrcode('');
        }
    }, [dispatch, data_connect]);

    return (
        <Modal id="modalLinkChannel" modal_size="modal-lg">
            <ModalHeader title="Connect Link Channel" />
            <ModalBody>
                <div className="border" style={{ height: 200, width: 200, margin: "0 auto" }}>
                    {
                        Boolean(qrcode.login_code) === true
                            ? <QRCode
                                size={256}
                                style={{ height: "auto", maxWidth: "100%", width: "100%" }}
                                value={qrcode.login_code}
                                viewBox={`0 0 256 256`}
                            />
                            : <QRCode
                                size={256}
                                style={{ height: "auto", maxWidth: "100%", width: "100%" }}
                                value=''
                                opacity={0.1}
                                viewBox={`0 0 256 256`}
                                disabled={true}
                            />
                    }
                </div>

                <h3 className="text-center my-10">Go to WhatsApp <b>Settings</b> {'>'} <b>Linked Devices</b> {'>'} <b>Scan QR Code.</b></h3>
            </ModalBody>
            <ModalFooter>
                <button type="button" onClick={CallApiQRCode} className="btn btn-sm btn-primary font-weight-bold m-1">Refresh QR Code</button>
            </ModalFooter>
        </Modal>
    )
}

export default ModalLinkChannel