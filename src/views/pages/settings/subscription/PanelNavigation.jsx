import React, { useState } from 'react'
import { baseurl_sosialapi } from 'app/config';
import IconBrand from 'views/components/IconBrand';
import SubscriptionChannelAdd from './SubscriptionChannelAdd';

function PanelNavigation({ openPopupWindow }) {
    const [channel, setChannel] = useState('');

    function openPopupWindow(url) {
        var w = 800;
        var h = 1000;
        var left = (screen.width / 2) - (w / 2);
        var top = (screen.height / 2) - (h / 2);
        return window.open(url, '_blank', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    }

    return (
        <div className="col-lg-4 col-md-6">
            <SubscriptionChannelAdd channel={channel} />

            <div className="card card-custom card-stretch border">
                <div className="card-header border-0 mt-5 mb-0">
                    <h3 className="card-title align-items-start flex-column">
                        <span className="card-label font-weight-bolder text-dark">Channel Integration</span>
                        <span className="text-muted mt-2 font-weight-bold font-size-sm">Click add connect to activation channel.</span>
                    </h3>
                </div>
                <div className="card-body py-0 px-5">
                    <div className="d-flex align-items-center border rounded mb-4">
                        <div className="symbol symbol-45 symbol-circle border-1 d-block m-4">
                            <div className="symbol-label">
                                <IconBrand name="Facebook" height={45} width={45} />
                            </div>
                        </div>
                        <div className="d-flex flex-column flex-grow-1 mr-2">
                            <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">Facebook</span>
                            <span className="text-muted font-weight-bold">Messenger, Mention, Feed</span>
                        </div>
                        {/* <button onClick={() => openPopupWindow(`${baseurl_sosialapi}/sosial/facebook/facebook/loginlink`)} className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel"> */}
                        <button type="button" onClick={() => setChannel('Facebook')} data-toggle="modal" data-target="#modalSubscriptionChannelAdd" className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel Facebook">
                            <i className="fas fa-user-plus icon-sm"></i>
                        </button>
                    </div>
                    <div className="d-flex align-items-center border rounded mb-4">
                        <div className="symbol symbol-45 symbol-circle border-1 d-block m-4">
                            <div className="symbol-label">
                                <IconBrand name="Instagram" height={45} width={45} />
                            </div>
                        </div>
                        <div className="d-flex flex-column flex-grow-1 mr-2">
                            <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">Instagram</span>
                            <span className="text-muted font-weight-bold">Direct Messages, Feed</span>
                        </div>
                        {/* <button onClick={() => openPopupWindow(`${baseurl_sosialapi}/sosial/instagram/instagram/loginlink`)} className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel"> */}
                        <button type="button" onClick={() => setChannel('Instagram')} data-toggle="modal" data-target="#modalSubscriptionChannelAdd" className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel Instagram">
                            <i className="fas fa-user-plus icon-sm"></i>
                        </button>
                    </div>
                    <div className="d-flex align-items-center border rounded mb-4">
                        <div className="symbol symbol-45 symbol-circle border-1 d-block m-4">
                            <div className="symbol-label">
                                <IconBrand name="Twitter" height={45} width={45} />
                            </div>
                        </div>
                        <div className="d-flex flex-column flex-grow-1 mr-2">
                            <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">Twitter</span>
                            <span className="text-muted font-weight-bold">Direct Messages, Mention</span>
                        </div>
                        {/* <button onClick={() => openPopupWindow(`${baseurl_sosialapi}/sosial/twitter/twitter/loginlink`)} className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel"> */}
                        <button type="button" onClick={() => setChannel('Twitter')} data-toggle="modal" data-target="#modalSubscriptionChannelAdd" className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel Twitter">
                            <i className="fas fa-user-plus icon-sm"></i>
                        </button>
                    </div>
                    <div className="d-flex align-items-center border rounded mb-4">
                        <div className="symbol symbol-45 symbol-circle border-1 d-block m-4">
                            <div className="symbol-label">
                                <IconBrand name="Whatsapp" height={45} width={45} />
                            </div>
                        </div>
                        <div className="d-flex flex-column flex-grow-1 mr-2">
                            <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">Whatsapp</span>
                            <span className="text-muted font-weight-bold">Private Messages</span>
                        </div>
                        <button type="button" onClick={() => setChannel('Whatsapp')} data-toggle="modal" data-target="#modalSubscriptionChannelAdd" className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel whatsapp">
                            <i className="fas fa-user-plus icon-sm"></i>
                        </button>
                    </div>
                    <div className="d-flex align-items-center border rounded mb-4">
                        <div className="symbol symbol-45 symbol-circle border-1 d-block m-4">
                            <div className="symbol-label">
                                <IconBrand name="Telegram" height={45} width={45} />
                            </div>
                        </div>
                        <div className="d-flex flex-column flex-grow-1 mr-2">
                            <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">Telegram</span>
                            <span className="text-muted font-weight-bold">Private Messages</span>
                        </div>
                        <button type="button" onClick={() => setChannel('Telegram')} data-toggle="modal" data-target="#modalSubscriptionChannelAdd" className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel Telegram">
                            <i className="fas fa-user-plus icon-sm"></i>
                        </button>
                    </div>
                    <div className="d-flex align-items-center border rounded mb-4">
                        <div className="symbol symbol-45 symbol-circle border-1 d-block m-4">
                            <div className="symbol-label">
                                <IconBrand name="Email" height={45} width={45} />
                            </div>
                        </div>
                        <div className="d-flex flex-column flex-grow-1 mr-2">
                            <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">Email</span>
                            <span className="text-muted font-weight-bold">Email Inbound - Outbound</span>
                        </div>
                        <button onClick={() => setChannel('Email')} data-toggle="modal" data-target="#modalSubscriptionChannelAdd" className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel email">
                            <i className="fas fa-user-plus icon-sm"></i>
                        </button>
                    </div>
                    {/* <div className="d-flex align-items-center border rounded mb-4">
                        <div className="symbol symbol-45 symbol-circle border-1 d-block m-4">
                            <div className="symbol-label">
                                <IconBrand name="Email" height={45} width={45} />
                            </div>
                        </div>
                        <div className="d-flex flex-column flex-grow-1 mr-2">
                            <span className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">WebChat</span>
                            <span className="text-muted font-weight-bold">Chat Messages</span>
                        </div>
                        <button className="btn btn-icon btn-light-primary btn-sm mr-4" title="add channel">
                            <i className="fas fa-user-plus icon-sm"></i>
                        </button>
                    </div> */}
                </div>
            </div>
        </div>
    )
}

export default PanelNavigation