import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import PanelNavigation from './PanelNavigation';
import ModalLinkChannel from './ModalLinkChannel';
import { ButtonRefresh } from 'views/components/button'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { apiSubcribedChannel, apiSubcribedChannelDelete } from 'app/services/apiSubscription';

function SubscriptionChannel() {
    const dispatch = useDispatch();
    const [data_connect, setDataConnect] = useState('');
    const { subcribed_channel } = useSelector(state => state.subscription);

    useEffect(() => {
        dispatch(apiSubcribedChannel())
    }, [dispatch]);

    function openWindow(url_api, channel) {
        const w = 800;
        const h = 1000;
        const left = (screen.width / 2) - (w / 2);
        const top = (screen.height / 2) - (h / 2);

        let url_connect = '';
        if (channel === 'Facebook' || channel === 'Instagram' || channel === 'Twitter') {
            url_connect = `${url_api}/sosial/${channel.toLowerCase()}/${channel.toLowerCase()}/loginlink`;
        }
        else {
            url_connect = url_api;
        }
        // Open a new window or tab with a specific URL
        const windowopen = window.open(url_connect, '_blank', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    }

    function onHandlerDelete(values) {
        Swal.fire({
            title: "Are you sure?",
            text: `${values.page_name} want to delete & unsubscribe this channel!`,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes"
        })
            .then(async function (res) {
                if (res.value) {
                    const { payload } = await dispatch(apiSubcribedChannelDelete(values));
                    if (payload.status === 200) {
                        Swal.fire({
                            title: "Success Delete.",
                            text: `deleted from database!`,
                            buttonsStyling: false,
                            icon: "success",
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                        dispatch(apiSubcribedChannel())
                    }
                }
            });
    }

    function componentButtonActions(data) {
        const values = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-start">
                <button type="button" onClick={() => onHandlerDelete(values)} className="btn btn-icon btn-light-danger btn-xs mx-1" title="Remove & Unsubscribe">
                    <i className="fas fa-ban icon-sm"></i>
                </button>
                <NavLink to={`/setting/subscription_edit?id=${values.id}&channel=${values.channel}&page_id=${values.page_id}`} className="btn btn-icon btn-light-warning btn-xs mx-1" title="Assign & Edit">
                    <i className="fas fa-edit icon-sm"></i>
                </NavLink>
                {
                    (values.channel === 'Whatsapp' && values.url_api !== 'https://graph.facebook.com/v17.0')
                        ? <button
                            type="button"
                            title="Scan QR Code"
                            onClick={() => setDataConnect(values)}
                            className={`btn btn-icon btn-xs mx-1 ${values.token ? 'btn-light-success' : 'btn-secondary'}`}
                            disabled={values.channel === 'Email' ? true : false}
                            data-toggle="modal"
                            data-target="#modalLinkChannel"
                        >
                            <i className="fas fa-qrcode icon-sm"></i>
                        </button>
                        : <button
                            type="button"
                            title={values.token ? 'Connected' : 'Connect Link'}
                            onClick={() => openWindow(values.url_api, values.channel)}
                            className={`btn btn-icon btn-xs mx-1 ${values.token ? 'btn-light-success' : 'btn-secondary'}`}
                            disabled={values.channel === 'Email' ? true : false}
                        >
                            <i className="fas fa-link icon-sm"></i>
                        </button>
                }
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="Setting" menu_name="Subscription Channel" modul_name="Activation account channel sosial media.">
                <ButtonRefresh onClick={() => dispatch(apiSubcribedChannel())} />
            </SubHeader>
            <Container>
                {data_connect && <ModalLinkChannel data_connect={data_connect} />}

                <div className="row">
                    <PanelNavigation />
                    <div className="col-lg-8 col-md-6">
                        <div className="card card-custom card-stretch border">
                            <div className="card-body p-5">
                                <h3 className="font-weight-bolder text-dark">Subscribed Account</h3>
                                <DataGrid
                                    dataSource={subcribed_channel}
                                    remoteOperations={{
                                        filtering: true,
                                        sorting: true,
                                        paging: true
                                    }}
                                    allowColumnReordering={true}
                                    allowColumnResizing={true}
                                    columnAutoWidth={true}
                                    showBorders={true}
                                    showColumnLines={true}
                                    showRowLines={true}
                                >
                                    <HeaderFilter visible={true} />
                                    <FilterRow visible={true} />
                                    <Paging defaultPageSize={5} />
                                    <Pager
                                        visible={true}
                                        allowedPageSizes={[5, 20, 50]}
                                        displayMode='full'
                                        showPageSizeSelector={true}
                                        showInfo={true}
                                        showNavigationButtons={true} />
                                    <GroupPanel visible={true} />
                                    <Column caption="Actions" dataField="id" fixed={true} width={120} cellRender={componentButtonActions} />
                                    <Column caption="Channel" dataField="channel" fixed={true} />
                                    <Column caption="Page ID" dataField="page_id" fixed={true} />
                                    <Column caption="Page Name" dataField="page_name" />
                                    <Column caption="Account" dataField="account_id" />
                                    <Column caption="Active" dataField="active" width={100} cellRender={({ data }) => {
                                        return <label className={`font-weight-bold ${data.active ? 'text-success' : 'text-danger'}`}>
                                            {data.active ? 'Yes' : 'No'}
                                        </label>
                                    }} />
                                    <Column caption="Create" dataField="created_at" dataType="datetime" format="yyyy-MM-dd hh:mm:ss" />
                                    <Column caption="Update" dataField="updated_at" dataType="datetime" format="yyyy-MM-dd hh:mm:ss" />
                                </DataGrid>
                            </div>
                        </div>

                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default SubscriptionChannel