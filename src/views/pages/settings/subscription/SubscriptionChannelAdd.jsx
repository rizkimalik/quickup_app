import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';

import FormInput from 'views/components/FormInput';
import { baseurl_sosialapi, url_docs } from 'app/config';
import { ButtonSubmit } from 'views/components/button'
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal'
import { apiSubcribedChannelInsert, apiSubcribedChannel } from 'app/services/apiSubscription';

function SubscriptionChannelAdd({ channel }) {
    const dispatch = useDispatch();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        reset({ channel });
        if (channel === 'Facebook' || channel === 'Instagram' || channel === 'Twitter') {
            reset({ channel, url_api: baseurl_sosialapi });
        }
    }, [reset, channel]);

    const onSubmitUpdateTicket = async (data) => {
        try {
            const { payload } = await dispatch(apiSubcribedChannelInsert(data));
            if (payload.status === 200) {
                SwalAlertSuccess('Success Insert', 'Success create new channel!');
                dispatch(apiSubcribedChannel());
            }
            else {
                SwalAlertError('Create Failed', 'Please Try again.');
            }
        } catch (error) {
            SwalAlertError('Create Failed', 'Please Try again, ' + error.message);
        }
    }

    return (
        <Modal id="modalSubscriptionChannelAdd" modal_size="modal-lg">
            <ModalHeader title="Connect New Channel" />
            <form onSubmit={handleSubmit(onSubmitUpdateTicket)} id="formAddChannel" name="formAddChannel">
                <ModalBody>
                    {
                        channel === 'Email'
                            ? <div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="username"
                                            type="email"
                                            label="Email"
                                            className="form-control"
                                            placeholder="Enter Email Server"
                                            register={register}
                                            rules={{ required: true, pattern: /^\S+@\S+$/i }}
                                            readOnly={false}
                                            errors={errors.username}
                                        />
                                    </div>
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="password"
                                            type="password"
                                            label="Password"
                                            className="form-control"
                                            placeholder="Enter Password"
                                            register={register}
                                            rules={{ required: true, maxLength: 100 }}
                                            readOnly={false}
                                            errors={errors.password}
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="port"
                                            type="number"
                                            label="Port"
                                            className="form-control"
                                            placeholder="Enter Port"
                                            register={register}
                                            rules={{ pattern: /^[0-9]+$/i }}
                                            readOnly={false}
                                            errors={errors.port}
                                        />
                                    </div>
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="tls"
                                            type="number"
                                            label="TLS"
                                            className="form-control"
                                            placeholder="Enter TLS"
                                            register={register}
                                            rules={{ pattern: /^[0-9]+$/i }}
                                            readOnly={false}
                                            errors={errors.tls}
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="host"
                                            type="text"
                                            label="Host Domain"
                                            className="form-control"
                                            placeholder="Enter Host Domain"
                                            register={register}
                                            rules={{ required: true }}
                                            readOnly={false}
                                            errors={errors.host}
                                        />
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <label>Email Type:</label>
                                                <select className="form-control" {...register("type", { required: true })}>
                                                    <option value="Inbound">Inbound</option>
                                                    <option value="Outbound">Outbound</option>
                                                </select>
                                                {errors.type && <span className="form-text text-danger">Select enter Type</span>}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2">
                                        <FormInput
                                            name="active"
                                            type="checkbox"
                                            label="Active"
                                            className="switch switch-outline switch-icon switch-primary ml-4"
                                            register={register}
                                            rules=""
                                            readOnly={false}
                                            errors={errors.active}
                                        />
                                    </div>
                                </div>
                            </div>

                            // except data email
                            : <div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="page_id"
                                            type="text"
                                            label="PageID"
                                            className="form-control"
                                            placeholder="Enter PageID"
                                            register={register}
                                            rules={{ required: true, maxLength: 200 }}
                                            readOnly={false}
                                            errors={errors.page_id}
                                        />
                                    </div>
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="page_name"
                                            type="text"
                                            label="Page name"
                                            className="form-control"
                                            placeholder="Enter Page Name"
                                            register={register}
                                            rules={{ required: true, maxLength: 200 }}
                                            readOnly={false}
                                            errors={errors.page_name}
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="channel"
                                            type="text"
                                            label="Channel"
                                            className="form-control"
                                            placeholder="Enter Channel"
                                            register={register}
                                            rules={{ required: true }}
                                            readOnly={true}
                                            errors={errors.channel}
                                        />
                                    </div>
                                    <div className="col-lg-6">
                                        <FormInput
                                            name="account_id"
                                            type="text"
                                            label="Account ID"
                                            className="form-control"
                                            placeholder="Enter Account ID"
                                            register={register}
                                            rules={{ required: true, maxLength: 200 }}
                                            readOnly={false}
                                            errors={errors.account_id}
                                        />
                                    </div>
                                </div>
                                <div className="hide">
                                    <FormInput
                                        name="url_api"
                                        type="text"
                                        label="Base URL API"
                                        className="form-control"
                                        placeholder="Enter Base URL API"
                                        register={register}
                                        rules={{ required: false, }}
                                        readOnly={false}
                                        errors={errors.url_api}
                                    />
                                    <FormInput
                                        name="token"
                                        type="textarea"
                                        label="Token Key"
                                        className="form-control"
                                        placeholder="Enter Token Key"
                                        register={register}
                                        rules={{ required: false, }}
                                        readOnly={false}
                                        errors={errors.token}
                                    />
                                    <FormInput
                                        name="token_secret"
                                        type="textarea"
                                        label="Secret Key"
                                        className="form-control"
                                        placeholder="Enter Secret Key"
                                        register={register}
                                        rules=""
                                        readOnly={false}
                                        errors={errors.token_secret}
                                    />
                                </div>
                                <FormInput
                                    name="active"
                                    type="checkbox"
                                    label="Active"
                                    className="switch switch-outline switch-icon switch-primary ml-4"
                                    register={register}
                                    rules=""
                                    readOnly={false}
                                    errors={errors.active}
                                />
                            </div>
                    }
                    <p>
                        Associate your {channel} company account with helpdesk to manage all queries coming in from there along with your support tickets. You can automatically convert visitor posts and private messages as tickets as well as use keyword rules to pull in specific comments as individual tickets.
                    </p>
                    <a href={url_docs + '/docs/subscription-channels'} target="_blank" className="text-primary"><i className="fas fa-link icon-sm text-primary" /> documentation</a>
                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default SubscriptionChannelAdd