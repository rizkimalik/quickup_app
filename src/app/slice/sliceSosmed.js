import { createSlice } from "@reduxjs/toolkit";
import { getListCustomer, getLoadConversation, getDataMessages, apiInteractionDetail, apiChatTemplate, apiAgentReadyAssign, apiListInboxFeeds, apiConversationFeed, apiShowDetailFeed } from 'app/services/apiSosmed'

const sosmedSlice = createSlice({
    name: "sosialmedia",
    initialState: {
        status: {
            connected: false,
            socket_id: null
        },
        selected_customer: {},
        list_customers: [],
        conversations: [],
        data_messages: [],
        interaction_detail: [],
        chat_templates: [],
        agent_ready_toassign: [],
        list_inbox_feeds: [],
        conversation_feed: [],
        detail_feed: {},
        selected_feed: {},
    },
    reducers: {
        setSocketStatus: (state, action) => {
            state.status = action.payload;
        },
        setSelectedCustomer: (state, action) => {
            state.selected_customer = action.payload;
        },
        setSelectedInboxFeed: (state, action) => {
            state.selected_feed = action.payload;
        },
        setDetailFeed: (state, action) => {
            state.detail_feed = action.payload;
        },
    },
    extraReducers: {
        [getListCustomer.fulfilled]: (state, action) => {
            state.list_customers = action.payload.data
        },
        [getLoadConversation.fulfilled]: (state, action) => {
            state.conversations = action.payload
        },
        [getDataMessages.fulfilled]: (state, action) => {
            state.data_messages = action.payload
        },
        [apiInteractionDetail.fulfilled]: (state, action) => {
            state.interaction_detail = action.payload.data
        },
        [apiChatTemplate.fulfilled]: (state, action) => {
            state.chat_templates = action.payload.data
        },
        [apiAgentReadyAssign.fulfilled]: (state, action) => {
            state.agent_ready_toassign = action.payload
        },
        [apiListInboxFeeds.fulfilled]: (state, action) => {
            state.list_inbox_feeds = action.payload.data
        },
        [apiConversationFeed.fulfilled]: (state, action) => {
            state.conversation_feed = action.payload
        },
        [apiShowDetailFeed.fulfilled]: (state, action) => {
            state.detail_feed = action.payload.data
        },
    },
});

//export actions & reducer
export const {
    setSocketStatus,
    setSelectedCustomer,
    setSelectedInboxFeed,
    setDetailFeed,
} = sosmedSlice.actions;
export default sosmedSlice;