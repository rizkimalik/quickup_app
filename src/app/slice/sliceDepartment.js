import { createSlice } from "@reduxjs/toolkit";
import { apiDepartmentDelete, apiDepartmentList, apiDepartmentShow, apiDepartmentStore, apiDepartmentUpdate } from "app/services/apiDepartment";


const sliceDepartment = createSlice({
    name: "department",
    initialState: {
        response: {},
        departments: [],
    },
    extraReducers: {
        [apiDepartmentList.fulfilled]: (state, action) => {
            state.departments = action.payload.data
        },
        [apiDepartmentShow.fulfilled]: (state, action) => {
            state.response = action.payload.data
        },
        [apiDepartmentStore.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiDepartmentUpdate.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiDepartmentDelete.fulfilled]: (state, action) => {
            state.response = action.payload
        },
    },
});

export default sliceDepartment;