import { createSlice } from "@reduxjs/toolkit";
import { apiChannelDelete, apiChannelList, apiChannelShow, apiChannelStore, apiChannelUpdate } from "app/services/apiChannel";


const sliceChannel = createSlice({
    name: "channel",
    initialState: {
        response: {},
        channels: [],
    },
    extraReducers: {
        [apiChannelList.fulfilled]: (state, action) => {
            state.channels = action.payload.data
        },
        [apiChannelShow.fulfilled]: (state, action) => {
            state.response = action.payload.data
        },
        [apiChannelStore.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiChannelUpdate.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiChannelDelete.fulfilled]: (state, action) => {
            state.response = action.payload
        },
    },
});

export default sliceChannel;