import { createSlice } from "@reduxjs/toolkit";
import { apiAuxActive, apiUpdateAuxUser,apiStatusAuxUser } from "app/services/apiAux";


const sliceAux = createSlice({
    name: "aux",
    initialState: {
        response: {},
        aux: [],
        aux_status: {},
    },
    extraReducers: {
        // [apiStatusList.fulfilled]: (state, action) => {
        //     state.status = action.payload.data
        // },
        // [apiStatusShow.fulfilled]: (state, action) => {
        //     state.response = action.payload.data
        // },
        [apiAuxActive.fulfilled]: (state, action) => {
            state.aux = action.payload.data
        },
        [apiUpdateAuxUser.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiStatusAuxUser.fulfilled]: (state, action) => {
            state.aux_status = action.payload.data
        },
        // [apiStatusDelete.fulfilled]: (state, action) => {
        //     state.response = action.payload
        // },
    },
});

export default sliceAux;