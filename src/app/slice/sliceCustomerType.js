import { createSlice } from "@reduxjs/toolkit";
import { apiCustomerTypeDelete, apiCustomerTypeList, apiCustomerTypeShow, apiCustomerTypeStore, apiCustomerTypeUpdate } from "app/services/apiCustomerType";


const sliceCustomerType = createSlice({
    name: "customer_type",
    initialState: {
        response: {},
        customer_type: [],
    },
    extraReducers: {
        [apiCustomerTypeList.fulfilled]: (state, action) => {
            state.customer_type = action.payload.data
        },
        [apiCustomerTypeShow.fulfilled]: (state, action) => {
            state.response = action.payload.data
        },
        [apiCustomerTypeStore.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiCustomerTypeUpdate.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiCustomerTypeDelete.fulfilled]: (state, action) => {
            state.response = action.payload
        },
    },
});

export default sliceCustomerType;