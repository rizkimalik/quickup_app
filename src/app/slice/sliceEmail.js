import { createSlice } from "@reduxjs/toolkit";
import { getEmailInboxData, getEmailSentData, apiEmailHistory, apiEmailAccount, apiEmailDetail, apiEmailDataInteraction, apiDataEmailSpam } from 'app/services/apiEmail'

const sliceEmail = createSlice({
    name: "email",
    initialState: {
        email_account: [],
        email_inbox: [],
        email_sent: [],
        email_history: [],
        email_interaction: [],
        email_data_detail: {},
        email_data_spam: [],
    },
    extraReducers: {
        [apiEmailAccount.fulfilled]: (state, action) => {
            state.email_account = action.payload.data
        },
        [apiEmailDetail.fulfilled]: (state, action) => {
            state.email_data_detail = action.payload.data
        },
        [getEmailInboxData.fulfilled]: (state, action) => {
            state.email_inbox = action.payload
        },
        [getEmailSentData.fulfilled]: (state, action) => {
            state.email_sent = action.payload
        },
        [apiEmailHistory.fulfilled]: (state, action) => {
            state.email_history = action.payload
        },
        [apiEmailDataInteraction.fulfilled]: (state, action) => {
            state.email_interaction = action.payload.data
        },
        [apiDataEmailSpam.fulfilled]: (state, action) => {
            state.email_data_spam = action.payload
        },
    },
});

export default sliceEmail;