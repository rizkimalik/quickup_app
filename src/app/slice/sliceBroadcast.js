import { createSlice } from "@reduxjs/toolkit";
import { apiWhatsappChannelList } from "app/services/apiBroadcast";

const sliceBroadcast = createSlice({
    name: "broadcast",
    initialState: {
        whatsapp_channel_list: [],
    },
    extraReducers: {
        [apiWhatsappChannelList.fulfilled]: (state, action) => {
            state.whatsapp_channel_list = action.payload.data
        },
    },
});

export default sliceBroadcast;