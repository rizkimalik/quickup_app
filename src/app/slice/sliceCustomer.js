import { createSlice } from "@reduxjs/toolkit";
import {
    apiCustomerList,
    apiCustomerShow,
    apiCustomerStore,
    apiCustomerUpdate,
    apiCustomerDelete,
    apiCustomerChannel,
    apiCustomerJourney,
    apiCustomerDxGrid,
    apiCustomer_DataChannel,
    apiCustomer_DataSync,
    apiCustomerRegistered,
} from "app/services/apiCustomer";

const sliceCustomer = createSlice({
    name: "customer",
    initialState: {
        customers: [],
        customer_registered: [],
        customer: {},
        response: {},
        channels: [],
        journey: [],
        dxcustomers: [],
        customer_channel: [],
        customer_syncronize: [],
    },
    extraReducers: {
        [apiCustomerList.fulfilled]: (state, action) => {
            state.customers = action.payload
        },
        [apiCustomerRegistered.fulfilled]: (state, action) => {
            state.customer_registered = action.payload
        },
        [apiCustomerShow.fulfilled]: (state, action) => {
            state.customer = action.payload.data
        },
        [apiCustomerStore.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiCustomerUpdate.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiCustomerDelete.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiCustomerChannel.fulfilled]: (state, action) => {
            state.channels = action.payload
        },
        [apiCustomerJourney.fulfilled]: (state, action) => {
            state.journey = action.payload.data
        },
        [apiCustomerDxGrid.fulfilled]: (state, action) => {
            state.dxcustomers = action.payload
        },
        [apiCustomer_DataChannel.fulfilled]: (state, action) => {
            state.customer_channel = action.payload
        },
        [apiCustomer_DataSync.fulfilled]: (state, action) => {
            state.customer_syncronize = action.payload
        },
    },
});

export default sliceCustomer;