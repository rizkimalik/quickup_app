import { createSlice } from "@reduxjs/toolkit";
import { apiStatusDelete, apiStatusList, apiStatusShow, apiStatusStore, apiStatusUpdate } from "app/services/apiStatus";


const sliceStatus = createSlice({
    name: "status",
    initialState: {
        response: {},
        status: [],
    },
    extraReducers: {
        [apiStatusList.fulfilled]: (state, action) => {
            state.status = action.payload.data
        },
        [apiStatusShow.fulfilled]: (state, action) => {
            state.response = action.payload.data
        },
        [apiStatusStore.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiStatusUpdate.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiStatusDelete.fulfilled]: (state, action) => {
            state.response = action.payload
        },
    },
});

export default sliceStatus;