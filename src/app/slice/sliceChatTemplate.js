import { createSlice } from "@reduxjs/toolkit";
import { apiChatTemplateDelete, apiChatTemplateList, apiChatTemplateShow, apiChatTemplateStore, apiChatTemplateUpdate } from "app/services/apiChatTemplate";


const sliceChatTemplate = createSlice({
    name: "chat_template",
    initialState: {
        response: {},
        chat_templates: [],
    },
    extraReducers: {
        [apiChatTemplateList.fulfilled]: (state, action) => {
            state.chat_templates = action.payload
        },
        [apiChatTemplateShow.fulfilled]: (state, action) => {
            state.response = action.payload.data
        },
        [apiChatTemplateStore.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiChatTemplateUpdate.fulfilled]: (state, action) => {
            state.response = action.payload
        },
        [apiChatTemplateDelete.fulfilled]: (state, action) => {
            state.response = action.payload
        },
    },
});

export default sliceChatTemplate;