import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { combineReducers } from "redux";

import user from './services/apiUser';
import menu_access from './services/apiMenu';
import sliceAuth from './slice/sliceAuth';
import sliceSosmed from './slice/sliceSosmed';
import sliceMenu from './slice/sliceMenu';
import sliceAux from './slice/sliceAux';
import sliceDashboard from './slice/sliceDashboard';
import sliceCustomer from './slice/sliceCustomer';
import sliceTicket from './slice/sliceTicket';
import sliceMasterData from './slice/sliceMasterData';
import sliceCategory from './slice/sliceCategory';
import sliceOrganization from './slice/sliceOrganization';
import sliceDepartment from './slice/sliceDepartment';
import sliceTodolist from './slice/sliceTodolist';
import sliceStatus from './slice/sliceStatus';
import sliceChannel from './slice/sliceChannel';
import sliceEmail from './slice/sliceEmail';
import sliceCall from './slice/sliceCall';
import sliceCustomerType from './slice/sliceCustomerType';
import slicePriorityScale from './slice/slicePriorityScale';
import sliceChatTemplate from './slice/sliceChatTemplate';
import sliceReport from './slice/sliceReport';
import sliceSubscription from './slice/sliceSubscription';
import sliceBroadcast from './slice/sliceBroadcast';

const persistConfig = {
    key: 'auth',
    version: 1,
    storage
}

const reducer = combineReducers({
    authUser: sliceAuth.reducer
})
const persistedReducer = persistReducer(persistConfig, reducer);

const rootReducer = {
    persistedReducer,
    mainmenu: sliceMenu.reducer,
    menu_access: menu_access.reducer,
    user: user.reducer,
    aux: sliceAux.reducer,
    sosialmedia: sliceSosmed.reducer,
    dashboard: sliceDashboard.reducer,
    customer: sliceCustomer.reducer,
    ticket: sliceTicket.reducer,
    master: sliceMasterData.reducer,
    category: sliceCategory.reducer,
    organization: sliceOrganization.reducer,
    department: sliceDepartment.reducer,
    todolist: sliceTodolist.reducer,
    status: sliceStatus.reducer,
    channel: sliceChannel.reducer,
    call: sliceCall.reducer,
    email: sliceEmail.reducer,
    customer_type: sliceCustomerType.reducer,
    priority_scale: slicePriorityScale.reducer,
    chat_template: sliceChatTemplate.reducer,
    report: sliceReport.reducer,
    subscription: sliceSubscription.reducer,
    broadcast: sliceBroadcast.reducer,
}

export default rootReducer;
