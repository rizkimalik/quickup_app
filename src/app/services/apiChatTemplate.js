import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from 'devextreme/data/custom_store';

export const apiChatTemplateList = createAsyncThunk(
    "chat_template/apiChatTemplateList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/chat_template`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiChatTemplateShow = createAsyncThunk(
    "chat_template/apiChatTemplateShow",
    async ({ id }) => {
        const res = await axios.get(`/chat_template/show/${id}`);
        return res.data;
    }
)

export const apiChatTemplateStore = createAsyncThunk(
    "chat_template/apiChatTemplateStore",
    async (params) => {
        const json = JSON.stringify(params);
        const res = await axios.post('/chat_template/store', json);
        return res.data;
    }
)

export const apiChatTemplateUpdate = createAsyncThunk(
    "chat_template/apiChatTemplateUpdate",
    async (params) => {
        const res = await axios.put('/chat_template/update', params);
        return res.data;
    }
)

export const apiChatTemplateDelete = createAsyncThunk(
    "chat_template/apiChatTemplateDelete",
    async ({ id }) => {
        const res = await axios.delete(`/chat_template/delete/${id}`);
        return res.data;
    }
)
