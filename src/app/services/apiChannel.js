import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiChannelList = createAsyncThunk(
    "channel/apiChannelList",
    async () => {
        const res = await axios.get('/channel');
        return res.data;
    }
)

export const apiChannelShow = createAsyncThunk(
    "channel/apiChannelShow",
    async ({ id }) => {
        const res = await axios.get(`/channel/show/${id}`);
        return res.data;
    }
)

export const apiChannelStore = createAsyncThunk(
    "channel/apiChannelStore",
    async (channel) => {
        const json = JSON.stringify(channel);
        const res = await axios.post('/channel/store', json);
        return res.data;
    }
)

export const apiChannelUpdate = createAsyncThunk(
    "channel/apiChannelUpdate",
    async (channel) => {
        const res = await axios.put('/channel/update', channel);
        return res.data;
    }
)

export const apiChannelDelete = createAsyncThunk(
    "channel/apiChannelDelete",
    async ({ id }) => {
        const res = await axios.delete(`/channel/delete/${id}`);
        return res.data;
    }
)
