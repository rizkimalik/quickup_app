import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiDepartmentList = createAsyncThunk(
    "department/apiDepartmentList",
    async () => {
        const res = await axios.get('/department');
        return res.data;
    }
)

export const apiDepartmentShow = createAsyncThunk(
    "department/apiDepartmentShow",
    async ({ department_id }) => {
        const res = await axios.get(`/department/show/${department_id}`);
        return res.data;
    }
)

export const apiDepartmentStore = createAsyncThunk(
    "department/apiDepartmentStore",
    async (department) => {
        const json = JSON.stringify(department);
        const res = await axios.post('/department/store', json);
        return res.data;
    }
)

export const apiDepartmentUpdate = createAsyncThunk(
    "department/apiDepartmentUpdate",
    async (department) => {
        const res = await axios.put('/department/update', department);
        return res.data;
    }
)

export const apiDepartmentDelete = createAsyncThunk(
    "department/apiDepartmentDelete",
    async ({ department_id }) => {
        const res = await axios.delete(`/department/delete/${department_id}`);
        return res.data;
    }
)
