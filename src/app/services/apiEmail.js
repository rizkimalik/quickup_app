import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";


export const apiInsertCustomerEmail = createAsyncThunk(
    "email/apiInsertCustomerEmail",
    async (param) => {
        const res = await axios.post('/omnichannel/email/insert_customer', JSON.stringify(param));
        return res.data;
    }
)

export const apiEmailAccount = createAsyncThunk(
    "email/apiEmailAccount",
    async ({ action }) => {
        const res = await axios.post(`/omnichannel/email/email_account?action=${action}`).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiEmailDetail = createAsyncThunk(
    "email/apiEmailDetail",
    async (param) => {
        const res = await axios.post('/omnichannel/email/email_detail', JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const getEmailInboxData = createAsyncThunk(
    "email/getEmailInboxData",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/email/data_inbox`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const getEmailSentData = createAsyncThunk(
    "email/getEmailSentData",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/email/data_sent`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiInsertEmailOut = createAsyncThunk(
    "email/apiInsertEmailOut",
    async (param) => {
        const res = await axios.post('/omnichannel/email/insert_email_out', JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiEmailHistory = createAsyncThunk(
    "email/apiEmailHistory",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/email/data_email_history`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiEmailDataInteraction = createAsyncThunk(
    "email/apiEmailDataInteraction",
    async (param) => {
        const res = await axios.post('/omnichannel/email/data_email_interaction', JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiInsertEmailSpam = createAsyncThunk(
    "email/apiInsertEmailSpam",
    async (param) => {
        const res = await axios.post('/omnichannel/email/insert_email_spam', JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiUpdateEmailUnSpam = createAsyncThunk(
    "email/apiUpdateEmailUnSpam",
    async (param) => {
        const res = await axios.post('/omnichannel/email/update_email_unspam', JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiDataEmailSpam = createAsyncThunk(
    "email/apiDataEmailSpam",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/email/data_email_spam`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)