import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { axiosDefault } from 'app/config';
axiosDefault();

export const apiAuthLogin = createAsyncThunk(
    "auth/apiAuthLogin",
    async (param) => {
        const res = await axios.post('/auth/login', JSON.stringify(param));
        return res.data;
    }
)

export const apiAuthLogout = createAsyncThunk(
    "auth/apiAuthLogout",
    async (param) => {
        const res = await axios.post('/auth/logout', JSON.stringify(param));
        return res.data;
    }
)

export const apiCheckAuthExpired = createAsyncThunk(
    "auth/apiCheckAuthExpired",
    async (param) => {
        const res = await axios.post('/auth/check_auth_expired', param);
        return res.data;
    }
)

export const apiKickLoginAuth = createAsyncThunk(
    "auth/apiKickLoginAuth",
    async (param) => {
        const res = await axios.post('/auth/login_kick', param);
        return res.data;
    }
)

