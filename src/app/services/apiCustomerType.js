import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiCustomerTypeList = createAsyncThunk(
    "customer_type/apiCustomerTypeList",
    async () => {
        const res = await axios.get('/customer_type');
        return res.data;
    }
)

export const apiCustomerTypeShow = createAsyncThunk(
    "customer_type/apiCustomerTypeShow",
    async ({ id }) => {
        const res = await axios.get(`/customer_type/show/${id}`);
        return res.data;
    }
)

export const apiCustomerTypeStore = createAsyncThunk(
    "customer_type/apiCustomerTypeStore",
    async (customer_type) => {
        const json = JSON.stringify(customer_type);
        const res = await axios.post('/customer_type/store', json);
        return res.data;
    }
)

export const apiCustomerTypeUpdate = createAsyncThunk(
    "customer_type/apiCustomerTypeUpdate",
    async (customer_type) => {
        const res = await axios.put('/customer_type/update', customer_type);
        return res.data;
    }
)

export const apiCustomerTypeDelete = createAsyncThunk(
    "customer_type/apiCustomerTypeDelete",
    async ({ id }) => {
        const res = await axios.delete(`/customer_type/delete/${id}`);
        return res.data;
    }
)
