import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiInsertCallHistory = createAsyncThunk(
    "call/apiInsertCallHistory",
    async (param) => {
        const json = JSON.stringify(param);
        const res = await axios.post('/webhook/call/get_history', json);
        return res.data;
    }
)

export const getCallHistoryData = createAsyncThunk(
    "call/getCallHistoryData",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/call/data_call`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch(() => { throw new Error('Data Loading..'); });
            },
        });

        return store;
    }
)


export const getCallTotal = createAsyncThunk(
    "call/getCallTotal",
    async () => {
        const res = await axios.post('/omnichannel/call/total_call');
        return res.data;
    }
)

export const getPabxToken = async () => {
    const res = await axios.post('/omnichannel/call/pabx_get_token').catch((error) => { return error.response });
    return res.data;
}

export const getPabxRecording = async (param) => {
    const res = await axios.post('/omnichannel/call/pabx_get_recording', JSON.stringify(param)).catch((error) => { return error.response });
    return res.data.data.items[0];
}
