import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
// import CustomStore from 'devextreme/data/custom_store';

export const apiBroadcastSend = createAsyncThunk(
    "broadcast/apiBroadcastSend",
    async (param) => {
        const formData = new FormData();
        formData.append('page_id', param.page_id);
        formData.append('broadcast_title', param.broadcast_title);
        formData.append('message_type', param.message_type);
        formData.append('message', param.message);
        formData.append('source', param.source);
        formData.append('phone_number', param.phone_number);
        formData.append('action', param.action);
        if (param.group_contact) {
            formData.append('group_contact', param.group_contact);
        }
        if (param.schedule_at) {
            formData.append('schedule_at', param.schedule_at);
        }
        if (param.excel_file) {
            formData.append('excel_file', param.excel_file[0]);
        }
        
        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const res = await axios.post('/broadcast/whatsapp/sendmessage', formData, config);
        return res.data;
    }
)

export const apiWhatsappChannelList = createAsyncThunk(
    "broadcast/apiWhatsappChannelList",
    async (param) => {
        const res = await axios.post('/broadcast/whatsapp/channel_list', param);
        return res.data;
    }
)