import React from "react";
import { Route, Redirect } from "react-router-dom";
import Guest from 'views/layouts/Guest';

const PublicRoute = ({ children, path, isAuth, ...rest }) => (
    <Route
        {...rest}
        path={path}
        render={({ location }) =>
            !isAuth ? (
                <Guest>{children}</Guest>
            ) : (
                <Redirect to={{ pathname: "/", state: { from: location } }} />
            )
        }
    />
);

export default React.memo(PublicRoute);
